/* eslint-disable react-native/no-inline-styles */
/* eslint-disable quotes */
/* eslint-disable prettier/prettier */

import React from "react";
import { LogBox, StatusBar } from "react-native";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { Provider } from "react-redux";
import RootNavigator from "./src/app-host/navigation/root-navigation";
import { store } from "./src/app-host/redux/store";
import { GestureHandlerRootView } from "react-native-gesture-handler";
// import FirebaseProvider from "./src/app-host/firebase/firebase";

LogBox.ignoreAllLogs(true);

const App = props => {
  return (
    <GestureHandlerRootView style={{ flex: 1 }}>
      <SafeAreaProvider>
        <Provider store={store}>
          {/* <FirebaseProvider> */}
          <StatusBar barStyle={"dark-content"} />
          <RootNavigator />
          {/* </FirebaseProvider> */}
        </Provider>
      </SafeAreaProvider>
    </GestureHandlerRootView>
  );
};

export default App;
