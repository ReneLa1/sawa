/* eslint-disable eol-last */
/* eslint-disable prettier/prettier */
/* eslint-disable no-trailing-spaces */
/* eslint-disable quotes */
// eslint-disable-next-line quotes

import { apiSlice } from "../../app-host/redux/api/api.slice";

export const stationsSlice = apiSlice.injectEndpoints({
  endpoints: build => ({
    getStartStations: build.query({
      query: () => ({
        url: `/mobile/start-point`,
        method: "GET",
      }),
      transformResponse: responseData => {
        return responseData.data;
      },
    }),
    getStopStations: build.query({
      query: () => ({
        url: `/mobile/end-point`,
        method: "GET",
      }),
      transformResponse: responseData => {
        return responseData.data;
      },
    }),
  }),
});

export const { useGetStartStationsQuery, useGetStopStationsQuery } =
  stationsSlice;

// returns the query result object
export const selectStartStationsResult =
  stationsSlice.endpoints.getStartStations.select();

export const selectStopStationsResult =
  stationsSlice.endpoints.getStopStations.select();
