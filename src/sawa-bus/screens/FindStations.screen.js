/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable quotes */
import { useNavigation, useRoute } from "@react-navigation/native";
import React, { useEffect, useRef, useState } from "react";
import { StyleSheet } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { ActionButton, Container, Input, Typography } from "../../common";
import GradientBackground from "../components/GradientBackground";
import SearchResults from "../components/SearchResults";
import {
  selectStartStationsResult,
  selectStopStationsResult,
} from "../redux/stations.slice";
import {
  addToHistory,
  setDepartStation,
  setDestStation,
} from "../redux/trip.slice";

export const FindStations = () => {
  const {
    params: { searchType },
  } = useRoute();
  const { trip } = useSelector(state => state.Trip);
  const startStations = useSelector(selectStartStationsResult).data;
  const stopStations = useSelector(selectStopStationsResult).data;

  const dispatch = useDispatch();
  const navigation = useNavigation();
  const departFieldRef = useRef(null);
  const destFieldRef = useRef(null);
  const [isFocused, setFocused] = useState(null);
  const [depart, setDepart] = useState(trip.from);
  const [dest, setDestination] = useState(trip.to ? trip.to : "");
  const [searchResult, setSearchResult] = useState([]);

  useEffect(() => {
    if (searchType === "from") {
      departFieldRef?.current.focus();
      setDepart("");
    }
    if (searchType === "to") {
      destFieldRef?.current.focus();
      setDestination("");
    }
    return () => {
      setSearchResult([]);
    };
  }, []);

  useEffect(() => {
    if (depart.length > 0) {
      getStations("from");
    }
    if (dest.length > 0) {
      getStations("to");
    }
  }, [depart, dest]);

  const getStations = type => {
    if (type === "from") {
      setSearchResult(
        startStations?.filter(station => {
          return station.from_name.toLowerCase().includes(depart.toLowerCase());
        }),
      );
    } else {
      setSearchResult(
        stopStations?.filter(station => {
          return station.end_name.toLowerCase().includes(dest.toLowerCase());
        }),
      );
    }
  };
  const handleFocus = name => setFocused(name);
  const handleBlur = () => setFocused(null);
  const focus_next = () => {
    destFieldRef?.current.focus();
  };
  const handleSubmitSearch = () => {
    if (isFocused === "dest" && dest.length > 0) {
      const approxResult = searchResult[0];
      approxResult && setDestination(approxResult.end_name);
      dispatch(setDestStation(approxResult.end_name));
      dispatch(addToHistory(approxResult.end_name));
      navigation.goBack();
    }
  };

  return (
    <Container customStyles={styles.containerStyles}>
      <GradientBackground />
      <Container flex={1} customStyles={{ zIndex: 3 }}>
        <Container
          row
          space="space-between"
          customStyles={{ paddingHorizontal: 20 }}>
          <Input
            clearTextOnFocus
            autoCapitalize={"words"}
            ref={departFieldRef}
            onFocus={() => handleFocus("depart")}
            onBlur={() => handleBlur(null)}
            placeholderTextColor="rgba(51,51,51,.9)"
            returnKeyType="next"
            inputStyles={styles.inputStyles}
            value={depart}
            onChangeText={txt => setDepart(txt)}
            placeholder="from"
            inputWrapperStyles={[
              styles.inputWrapperStyles,
              {
                backgroundColor:
                  isFocused === "depart"
                    ? "rgba(248, 249, 249,0.8)"
                    : "rgba(244, 246, 246,0.8)",

                borderColor:
                  isFocused === "depart"
                    ? "rgba(49, 172, 101,0.6)"
                    : "transparent",
              },
            ]}
            customStyles={{ width: "82%" }}
            blurOnSubmit={false}
            onSubmitEditing={() => {
              if (isFocused === "depart" && depart.length > 0) {
                const approxResult = searchResult[0];
                approxResult && setDepart(approxResult.from_name);
                dispatch(setDepartStation(approxResult.from_name));
                focus_next();
                dispatch(addToHistory(approxResult.from_name));
              }
            }}
          />
          <ActionButton onPress={() => navigation.goBack()}>
            <Typography variant="body" customStyles={{ fontSize: 16 }}>
              Cancel
            </Typography>
          </ActionButton>
        </Container>
        <Container
          row
          space="space-between"
          customStyles={{ paddingHorizontal: 20 }}>
          <Input
            ref={destFieldRef}
            clearTextOnFocus
            autoCapitalize={"words"}
            onFocus={() => handleFocus("dest")}
            onBlur={() => handleBlur(null)}
            placeholderTextColor="rgba(51,51,51,.9)"
            inputStyles={styles.inputStyles}
            value={dest}
            onChangeText={txt => setDestination(txt)}
            placeholder="arrival station"
            blurOnSubmit={false}
            inputWrapperStyles={[
              styles.inputWrapperStyles,
              {
                backgroundColor:
                  isFocused === "dest"
                    ? "rgba(248, 249, 249,0.8)"
                    : "rgba(244, 246, 246,0.8)",

                borderColor:
                  isFocused === "dest"
                    ? "rgba(49, 172, 101,0.6)"
                    : "transparent",
              },
            ]}
            customStyles={{ width: "82%" }}
            onSubmitEditing={handleSubmitSearch}
          />
        </Container>

        <Container flex={1}>
          <SearchResults
            stations={searchResult}
            setDepart={setDepart}
            focus_next={focus_next}
          />
        </Container>
      </Container>
    </Container>
  );
};

export default FindStations;
const styles = StyleSheet.create({
  containerStyles: {
    flex: 1,
    paddingTop: 20,
  },
  inputStyles: {
    color: "#000",
    fontSize: 15,
    fontFamily: "ArialRoundedMTBold",
  },
  inputWrapperStyles: {
    borderRadius: 5,
    height: 40,
    paddingHorizontal: 10,
    borderWidth: 1.3,
  },
});
