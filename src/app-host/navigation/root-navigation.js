/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable quotes */
/* eslint-disable prettier/prettier */

import firestore from "@react-native-firebase/firestore";
import { NavigationContainer } from "@react-navigation/native";
import React, { useCallback, useEffect } from "react";
import SInfo from "react-native-sensitive-info";
import { useDispatch } from "react-redux";
import { navigationRef } from "../../common/utils/utils";
import { setCredentials } from "../redux/user/auth.slice";
import { AppNavigator } from "./app.navigator";
import { saveServices } from "../redux/services/services.slice";

const RootNavigator = () => {
  const dispatch = useDispatch();

  const getServices = async () => {
    await firestore()
      .collection("services")
      .get()
      .then(querySnapshot => {
        let newServices = [];
        querySnapshot.forEach(documentSnapshot => {
          const name = documentSnapshot.data().name;

          // console.log(name);
          newServices.push(documentSnapshot.data());
        });
        dispatch(saveServices(newServices));
      });
  };

  const handleGetUser = useCallback(async () => {
    try {
      const data = await SInfo.getItem("auth", {
        sharedPreferencesName: "sawaApp",
        keychainService: "sawaApp",
      });
      const user = JSON.parse(data);
      dispatch(setCredentials(user));
    } catch (err) {
      console.log("User: ", err);
    }
  }, [dispatch]);

  useEffect(() => {
    handleGetUser();
    getServices();
  }, [handleGetUser]);

  return (
    <NavigationContainer ref={navigationRef} headerMode="none">
      <AppNavigator />
    </NavigationContainer>
  );
};

export default RootNavigator;
