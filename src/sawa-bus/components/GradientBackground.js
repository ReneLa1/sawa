/* eslint-disable quotes */
import { StyleSheet, Dimensions } from "react-native";
import React from "react";
import LinearGradient from "react-native-linear-gradient";

const { height } = Dimensions.get("window");

export default () => {
  return (
    <LinearGradient
      colors={[
        "rgba(169, 204, 227,0.8)",
        "rgba(169, 204, 227,0.5)",
        "rgba(169, 204, 227,0.01)",
      ]}
      style={styles.linearGradientBackdrop}
    />
  );
};

const styles = StyleSheet.create({
  linearGradientBackdrop: {
    position: "absolute",
    height: height / 2.5,
    top: 0,
    left: 0,
    right: 0,
    zIndex: 0,
  },
});
