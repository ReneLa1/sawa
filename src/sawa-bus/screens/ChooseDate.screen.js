/* eslint-disable react-native/no-inline-styles */
/* eslint-disable quotes */
import { useNavigation } from "@react-navigation/native";
import React, { useState } from "react";
import { StyleSheet } from "react-native";
import DatePicker from "react-native-date-picker";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import Feather from "react-native-vector-icons/Feather";
import { useDispatch } from "react-redux";
import {
  ActionButton,
  Container,
  PrimaryButton,
  Typography,
} from "../../common";
import GradientBackground from "../components/GradientBackground";
import { chooseDate } from "../redux/trip.slice";

const ChooseDate = () => {
  const insets = useSafeAreaInsets();
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const [date, setDate] = useState(new Date());

  const onSaveDate = () => {
    dispatch(chooseDate(date));
    navigation.goBack();
  };

  return (
    <Container
      customStyles={[styles.containerStyles, { paddingTop: insets.top - 10 }]}>
      <GradientBackground />
      <ActionButton
        activeScale={0.95}
        tension={50}
        friction={7}
        useNativeDriver
        customStyles={{ maxWidth: 30, padding: 0 }}
        onPress={() => navigation.goBack()}>
        <Feather name="arrow-left" size={25} color="#000" />
      </ActionButton>
      <Container flex={0.9} column space="space-between">
        <Typography variant="h1" customStyles={styles.titleStyles}>
          Select travel date
        </Typography>
        <Container>
          <DatePicker
            date={date}
            mode="datetime"
            onDateChange={setDate}
            minimumDate={new Date()}
            textColor="#000"
          />
        </Container>
        <Container customStyles={{ paddingHorizontal: 10 }}>
          <PrimaryButton
            onPress={onSaveDate}
            //#31AC65
            bgColor={"rgba(49, 172, 101,0.7)"}
            activeScale={0.95}
            tension={50}
            friction={7}
            useNativeDriver
            customStyles={styles.btnStyles}>
            <Typography
              variant="caption"
              customStyles={{
                color: "#fff",
                fontSize: 17,
              }}>
              Save
            </Typography>
          </PrimaryButton>
        </Container>
      </Container>
    </Container>
  );
};

export default ChooseDate;
const styles = StyleSheet.create({
  containerStyles: {
    flex: 1,
    paddingHorizontal: 20,
  },
  titleStyles: {
    fontSize: 23,
    marginTop: 20,
  },
  btnStyles: {
    borderRadius: 25,
    paddingTop: 15,
    paddingBottom: 15,
    marginBottom: 20,
  },
});
