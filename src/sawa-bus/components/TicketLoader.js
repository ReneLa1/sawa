/* eslint-disable quotes */
/* eslint-disable react/react-in-jsx-scope */

import React from "react";
import ContentLoader, { Rect, Circle } from "react-content-loader/native";
import { Container } from "../../common";
import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";

const TicketLoader = () => (
  <Container column customStyles={styles.containerStyles}>
    <ContentLoader viewBox="0 0 400 140">
      <Rect x="0" y="0" rx="2" ry="2" width="70" height="25" />
      {/* time details*/}
      <Rect x="0" y="50" rx="2" ry="2" width="80" height="15" />
      <Rect x="100" y="50" rx="2" ry="2" width="80" height="15" />
      <Rect x="220" y="50" rx="2" ry="2" width="80" height="15" />
      {/* stations details*/}
      <Rect x="0" y="75" rx="2" ry="2" width="60" height="15" />
      <Rect x="100" y="75" rx="2" ry="2" width="60" height="15" />
      <Rect x="220" y="75" rx="2" ry="2" width="60" height="15" />
      <Circle x="340" y="50" cx="22" cy="22" r="22" />
      <Rect x="0" y="120" rx="2" ry="2" width="70" height="25" />
    </ContentLoader>
  </Container>
);

export default TicketLoader;
const styles = StyleSheet.create({
  containerStyles: {
    width: "100%",
    height: hp(19),
    backgroundColor: "#fff",
    borderRadius: 5,
    paddingHorizontal: 10,
    marginBottom: 10,
  },
});
