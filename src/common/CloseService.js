/* eslint-disable quotes */
import { useNavigation } from "@react-navigation/native";
import React from "react";
import { StyleSheet } from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import Feather from "react-native-vector-icons/Feather";
import { ActionButton, Container } from "./";

export default () => {
  const navigation = useNavigation();
  return (
    <Container
      row
      center
      space="space-around"
      customStyles={styles.closeBtnStyles}>
      <ActionButton>
        <Feather name="more-horizontal" size={20} color="black" />
      </ActionButton>
      <ActionButton onPress={() => navigation.goBack()}>
        <AntDesign name="closecircleo" size={20} color="black" />
      </ActionButton>
    </Container>
  );
};

const styles = StyleSheet.create({
  closeBtnStyles: {
    height: 30,
    backgroundColor: "#fff",
    paddingHorizontal: 4,
    paddingVertical: 4,
    borderRadius: 20,
  },
});
