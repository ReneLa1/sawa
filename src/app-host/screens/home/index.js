/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable quotes */
import React, { useRef, useContext, useEffect } from "react";
import { Dimensions } from "react-native";
import { Gesture } from "react-native-gesture-handler";
import { useSharedValue, withSpring } from "react-native-reanimated";
import { Container } from "../../../common";
import MapWrapper from "../../components/MapWrapper";
import ServicesWrapper from "../../components/ServicesWrapper";
import Map from "./Map";

const { height: SCREEN_HEIGHT } = Dimensions.get("window");
const MAX_TRANSLATE_Y = -SCREEN_HEIGHT + 50;

const Home = () => {
  const translateY = useSharedValue(-SCREEN_HEIGHT / 2.2);
  const context = useSharedValue({ y: 0 });
  const gesture = Gesture.Pan()
    .onStart(() => {
      context.value = { y: translateY.value };
    })
    .onUpdate(evt => {
      translateY.value = evt?.translationY + context.value.y;
      translateY.value = Math.max(translateY.value, MAX_TRANSLATE_Y + 150);
      translateY.value = Math.min(translateY.value, MAX_TRANSLATE_Y / 2);
    })
    .onEnd(() => {
      if (translateY.value < -SCREEN_HEIGHT / 2) {
        translateY.value = withSpring(-SCREEN_HEIGHT / 2, { damping: 50 });
      }
    });
  const ref = useRef(null);

  return (
    <Container flex={1} bgColor="#F2F8FF">
      <MapWrapper translateY={translateY}>
        <Map />
      </MapWrapper>
      <ServicesWrapper
        translateY={translateY}
        context={context}
        gesture={gesture}
        ref={ref}
        // onPress={onPress}
      />
    </Container>
  );
};

export default Home;
