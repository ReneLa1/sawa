/* eslint-disable quotes */
import AsyncStorage from "@react-native-async-storage/async-storage";
import SInfo from "react-native-sensitive-info";

const USER = "auth";

export const on_login = user => {
  SInfo.setItem(USER, JSON.stringify(user), {});
};
// export const getUser = await SInfo.getItem(USER, {});

// export const on_logout = () => AsyncStorage.removeItem(USER);
