/* eslint-disable react/jsx-no-comment-textnodes */
/* eslint-disable prettier/prettier */
/* eslint-disable quotes */
import { useRoute } from "@react-navigation/native";
import {
  CardStyleInterpolators,
  createStackNavigator,
} from "@react-navigation/stack";
import React from "react";
import { useSelector } from "react-redux";
import Checkout from "../screens/Checkout.screen";
import ChooseDate from "../screens/ChooseDate.screen";
import FindStations from "../screens/FindStations.screen";
import BusHome from "../screens/index";
import ScheduledBuses from "../screens/ScheduledBuses.screen";
import { BusRoutes } from "./bus.routes";

const Stack = createStackNavigator();

export const BusStack = () => {
  const { theme } = useSelector(({ Utils }) => Utils);
  const {
    params: { service },
  } = useRoute();
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        cardStyle: {
          backgroundColor: theme.SECONDARY_BACKGROUND_COLOR,
        },
      }}
      mode="modal"
      headerMode="none">
      <Stack.Screen
        name={BusRoutes.SAWA_BUS}
        component={BusHome}
        initialParams={{ service }}
      />
      <Stack.Screen
        name={BusRoutes.SEARCH_STATIONS}
        component={FindStations}
        options={{
          animationEnabled: true,
        }}
        initialParams={{ service }}
      />
      <Stack.Screen
        name={BusRoutes.CHOOSE_DATE}
        component={ChooseDate}
        options={{
          animationEnabled: true,
        }}
      />
      <Stack.Screen
        name={BusRoutes.AVAILABLE_BUSES}
        component={ScheduledBuses}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
      <Stack.Screen
        name={BusRoutes.CHECKOUT_TICKET}
        component={Checkout}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forModalPresentationIOS,
          // animationEnabled: false,
        }}
      />
    </Stack.Navigator>
  );
};
export default BusStack;
