/* eslint-disable quotes */
import actionTypes from "./types";

export const showSpinner = () => ({ type: actionTypes.SHOW_SPINNER });
export const hideSpinner = () => ({ type: actionTypes.HIDE_SPINNER });
export const loginInProgress = () => ({ type: actionTypes.LOGIN_IN_PROGRESS });
export const loginComplete = () => ({ type: actionTypes.LOGIN_COMPLETE });
export const registerInProgress = () => ({
  type: actionTypes.SIGNUP_IN_PROGRESS,
});
export const registerComplete = () => ({ type: actionTypes.SIGN_UP_COMPLETE });

export const userInValid = () => ({
  type: actionTypes.USER_INVALID,
});

export const clearData = (prop, val) => ({
  type: actionTypes.CLEAR_DATA,
  payload: {
    prop,
    val,
  },
});
