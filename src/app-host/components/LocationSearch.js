/* eslint-disable react-native/no-inline-styles */
/* eslint-disable quotes */

import React from "react";
import { StyleSheet, Text } from "react-native";
import { ActionButton, Container } from "../../common";
import Feather from "react-native-vector-icons/Feather";
import AntDesign from "react-native-vector-icons/AntDesign";

const SearchIcon = <Feather name="search" size={22} color="#1A459C" />;
const CaseIcon = (
  <Feather name="briefcase" size={17} color="rgba(26, 69, 156,.7)" />
);
const HomeIcon = (
  <AntDesign name="home" size={18} color="rgba(26, 69, 156,.7)" />
);

const LocationSearch = ({ onPress }) => {
  return (
    <Container bgColor="#F2F8FF" column customStyles={styles.containerStyles}>
      <Container customStyles={styles.searchWrapperStyles}>
        <ActionButton
          onPress={onPress}
          activeScale={0.95}
          tension={50}
          friction={7}
          useNativeDriver
          customStyles={styles.searchButtonStyles}>
          {SearchIcon}
          <Container
            row
            center
            customStyles={{ flex: 1, marginLeft: 10, flexGrow: 1 }}>
            <Text style={styles.searchTextStyles}>I want to go to...</Text>
          </Container>
        </ActionButton>
      </Container>
      <Container row center customStyles={styles.actionsWrapperStyles}>
        {Button(HomeIcon, "Home", {})}
        {Button(CaseIcon, "Work", {
          marginLeft: 5,
          marginRight: 5,
          borderLeftWidth: 1,
          borderLeftColor: "rgba(0,0,0,.3)",
          borderRightWidth: 0.7,
          borderRightColor: "rgba(0,0,0,.3)",
          paddingLeft: 15,
          paddingRight: 15,
        })}
        {Button(null, "Places ...", {})}
      </Container>
    </Container>
  );
};

const Button = (icon, title, style) => {
  return (
    <ActionButton
      customStyles={[styles.buttonStyles, style]}
      activeScale={0.95}
      tension={50}
      friction={7}
      useNativeDriver>
      {icon && icon}
      <Text
        style={{
          fontSize: 15,
          fontWeight: "600",
          paddingLeft: 7,
          color: "rgba(0,0,0,.5)",
          fontFamily: "ArialRoundedMTBold",
        }}>
        {title}
      </Text>
    </ActionButton>
  );
};

export default LocationSearch;

const styles = StyleSheet.create({
  containerStyles: {
    position: "absolute",
    left: 10,
    right: 10,
    top: -20,
    borderRadius: 5,
    paddingVertical: 5,
    paddingHorizontal: 15,
  },
  searchWrapperStyles: {
    borderBottomColor: "#e6e8ec",
    borderBottomWidth: 1,
  },
  searchButtonStyles: {
    flexDirection: "row",
    justifyContent: "flex-start",
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingTop: 10,
    // backgroundColor: "red",
  },
  searchTextStyles: {
    color: "#000",
    fontWeight: "600",
    fontFamily: "ArialRoundedMTBold",
  },

  actionsWrapperStyles: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingTop: 12,
  },
  buttonStyles: {
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "flex-start",
    marginRight: 10,
    // backgroundColor: "green",
  },
});
