/* eslint-disable react-native/no-inline-styles */
/* eslint-disable quotes */
import { addMinutes, format } from "date-fns";
import React from "react";
import { StyleSheet, Text } from "react-native";
import Animated, { FadeInDown } from "react-native-reanimated";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { ActionButton, Container, Typography } from "../../common";
import { timeConvert } from "../utils/timeConvert";
import { useNavigation } from "@react-navigation/native";
import { BusRoutes } from "../navigation/bus.routes.js";
import { onChooseTicket } from "../redux/trip.slice";
import { useDispatch } from "react-redux";

const BusTicket = ({ bus }) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const arrival_time = format(
    addMinutes(new Date(bus.start_time), bus.duration),
    "HH:mm",
  );
  const onSelectTicket = () => {
    dispatch(onChooseTicket({ prop: "ticket", value: bus }));
    navigation.navigate(BusRoutes.CHECKOUT_TICKET);
  };
  return (
    <Animated.View
      entering={FadeInDown.springify().delay(5)}
      style={{ width: "100%", height: hp(18) }}>
      <ActionButton
        onPress={onSelectTicket}
        customStyles={styles.containerStyles}
        activeScale={0.95}
        tension={50}
        friction={7}
        useNativeDriver>
        <Container row center customStyles={{ marginBottom: 5 }}>
          <Text
            style={{
              color: "rgba(49, 172, 101,225)",
              fontSize: 14,
              fontWeight: "600",
              fontFamily: "ArialRoundedMTBold",
              marginRight: 3,
            }}>
            rwf
          </Text>
          <Typography variant="title" customStyles={styles.priceTextStyles}>
            {bus.price}
          </Typography>
        </Container>
        {/* ticket details*/}

        <Container
          row
          center
          space="space-between"
          customStyles={{ width: "100%" }}>
          <Container column>
            <Typography customStyles={styles.primaryTextStyles}>
              {bus.time}
            </Typography>
            <Text style={styles.secTextStyles}>{bus.from}</Text>
          </Container>
          <Container>
            <Typography customStyles={styles.primaryTextStyles}>
              {arrival_time}
            </Typography>
            <Text style={styles.secTextStyles}>{bus.to}</Text>
          </Container>
          <Container column middle center>
            <Typography
              customStyles={[styles.primaryTextStyles, { fontSize: 16 }]}>
              {timeConvert(bus.duration)}
            </Typography>
            <Text style={styles.secTextStyles}>
              {`${bus.route_name.split("-").length} stations`}
            </Text>
          </Container>
          <Container
            customStyles={{ height: 22, width: 22, borderRadius: 22 / 2 }}>
            <Typography>{""}</Typography>
          </Container>
        </Container>

        {/* company details*/}
        <Container
          row
          center
          // bgColor="red"
          space="space-between"
          customStyles={{
            width: "100%",
            borderTopWidth: 1,
            borderTopColor: "rgba(49, 172, 101,0.4)",
            paddingTop: 3,
            marginTop: 13,
          }}>
          <Container row center>
            <Typography
              customStyles={[styles.primaryTextStyles, { fontSize: 15 }]}>
              {bus.company}
            </Typography>
          </Container>
          <Container row center>
            <Text style={[styles.secTextStyles, {}]}>Seats: </Text>
            <Typography
              variant="title"
              customStyles={[
                styles.secTextStyles,
                { color: "rgba(49, 172, 101,225)" },
              ]}>
              {bus.seats_available}
            </Typography>
          </Container>
        </Container>
      </ActionButton>
    </Animated.View>
  );
};

export default BusTicket;

const styles = StyleSheet.create({
  containerStyles: {
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "space-around",
    flex: 1,
    backgroundColor: "#fff",
    borderRadius: 5,
    paddingLeft: 20,
    paddingRight: 20,
    marginBottom: 10,
    paddingTop: 10,
    paddingBottom: 10,
  },
  priceTextStyles: {
    fontSize: 19,
    color: "rgba(49, 172, 101,225)",
  },
  primaryTextStyles: {
    fontSize: 17,
    marginBottom: 5,
  },
  secTextStyles: {
    fontSize: 14,
    fontWeight: "500",
    color: "rgba(156,156,156,255)",
  },
});
