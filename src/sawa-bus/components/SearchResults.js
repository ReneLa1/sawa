/* eslint-disable dot-notation */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable quotes */
import React from "react";
import { FlatList, StyleSheet, Keyboard } from "react-native";
import { ActionButton, Typography } from "../../common";
import { useDispatch, useSelector } from "react-redux";
import {
  setDepartStation,
  setDestStation,
  addToHistory,
} from "../redux/trip.slice";
import { useNavigation } from "@react-navigation/native";

const SearchResults = ({ stations, setDepart, focus_next }) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const { trip } = useSelector(state => state.Trip);

  const handleSelect = (type, item) => {
    if (type) {
      dispatch(setDepartStation(item.from_name));
      setDepart(item.from_name);
      focus_next();
      dispatch(addToHistory(item.from_name));
    } else {
      dispatch(setDestStation(item.end_name));
      dispatch(addToHistory(item.end_name));
      navigation.goBack();
    }
  };
  return (
    <FlatList
      data={stations}
      keyboardShouldPersistTaps="always"
      keyExtractor={i => i}
      onScrollBeginDrag={Keyboard.dismiss}
      style={{ flex: 1 }}
      contentContainerStyle={{ paddingTop: 20, paddingBottom: 50 }}
      renderItem={({ item, index }) => {
        const is_first = index === 0;
        const isDepart = item.from_name;
        return (
          <ActionButton
            onPress={() => {
              handleSelect(isDepart, item);
            }}
            activeScale={0.95}
            tension={50}
            friction={7}
            useNativeDriver
            customStyles={[
              styles.itemStyles,
              {
                backgroundColor: is_first
                  ? "rgba(49, 172, 101,0.1)"
                  : "transparent",
              },
            ]}>
            <Typography customStyles={styles.nameTextStyles}>
              {item.from_name || item.end_name}
            </Typography>
          </ActionButton>
        );
      }}
    />
  );
};

export default SearchResults;
const styles = StyleSheet.create({
  itemStyles: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 13,
    paddingBottom: 13,
    flexDirection: "row",
    justifyContent: "flex-start",
  },
  nameTextStyles: {
    color: "#000",
    fontSize: 15,
  },
});
