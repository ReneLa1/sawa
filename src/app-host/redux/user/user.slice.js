/* eslint-disable eol-last */
/* eslint-disable prettier/prettier */
/* eslint-disable no-trailing-spaces */
/* eslint-disable quotes */
// eslint-disable-next-line quotes

import SInfo from "react-native-sensitive-info";
import { apiSlice } from "../api/api.slice";

export const userSlice = apiSlice.injectEndpoints({
  endpoints: builder => ({
    loginUser: builder.mutation({
      query: userCredentials => ({
        url: "/mobile/authenticate",
        method: "POST",
        body: {
          phone: userCredentials.phone,
          password: userCredentials.password,
        },
        headers: {
          platform: "android",
          app_version: "v2",
        },
      }),
      transformResponse: responseData => {
        const { data } = responseData;
        SInfo.setItem("auth", JSON.stringify(data), {
          sharedPreferencesName: "sawaApp",
          keychainService: "sawaApp",
        });
      },
      invalidatesTags: [{ type: "User", id: "AUTH_LOG" }],
    }),
    registerUser: builder.mutation({
      query: userCredentials => ({
        url: "/mobile/register",
        method: "POST",
        body: {
          first_name: userCredentials.first_name,
          last_name: userCredentials.last_name,
          email: userCredentials.email,
          password: userCredentials.password,
          phone: userCredentials.phone,
        },
        headers: {
          os: "android",
        },
      }),
      invalidatesTags: [{ type: "User", id: "AUTH_SIGN" }],
    }),
    updateUser: builder.mutation({
      query: user => ({
        url: `/mobile/user/${user.user_id}`,
        method: "PUT",
        body: {
          first_name: user.first_name,
          last_name: user.last_name,
        },
        headers: {
          os: "android",
          Authorization: `Bearer ${user.token}`,
        },
      }),
      invalidatesTags: (result, error, arg) => [
        { type: "User", id: arg.user_id },
      ],
    }),
  }),
});

export const {
  useLoginUserMutation,
  useRegistrationUserMutation,
  useUpdateUserMutation,
} = userSlice;
