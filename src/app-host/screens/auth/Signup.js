/* eslint-disable react/jsx-no-comment-textnodes */
/* eslint-disable no-trailing-spaces */
/* eslint-disable react/self-closing-comp */
/* eslint-disable prettier/prettier */
/* eslint-disable quotes */
/* eslint-disable react-native/no-inline-styles */

import React, { useEffect, useState } from "react";
import {
  View,
  Keyboard,
  StyleSheet,
  Platform,
  ActivityIndicator,
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import {
  Container,
  Typography,
  ActionButton,
  Input,
  PrimaryButton,
} from "../../../common";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { useNavigation } from "@react-navigation/native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { AppRoutes } from "../../navigation/app.routes";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Fontisto from "react-native-vector-icons/Fontisto";
import Ionicons from "react-native-vector-icons/Ionicons";
import Feather from "react-native-vector-icons/Feather";
import Entypo from "react-native-vector-icons/Entypo";
import { useRegistrationUserMutation } from "../../redux/user/user.slice";

const CardIcon = (
  <FontAwesome name="credit-card-alt" size={24} color="rgb(125, 206, 160)" />
);

const MotoIcon = <Fontisto name="motorcycle" size={24} color="#3498DB" />;

const CarIcon = (
  <Ionicons name="car-sport-sharp" size={28} color="rgba(239, 70, 111,0.9)" />
);
const BusIcon = <Fontisto name="bus" size={24} color="rgb(52, 152, 219 )" />;
const PhoneIcon = <Feather name="phone" size={24} color="#3498DB" />;
const LockIcon = <Feather name="lock" size={24} color="#3498DB" />;
const CloseIcon = <Entypo name="chevron-left" size={24} color="#fff" />;
const UserIcon = <Feather name="user" size={24} color="#3498DB" />;
const EmailIcon = <Feather name="mail" size={24} color="#3498DB" />;

const Signup = () => {
  // const [registerUser, { isLoading, isSuccess }] =
  //   useRegistrationUserMutation();

  const navigation = useNavigation();
  const insets = useSafeAreaInsets();
  const [keyShown, setKeyboardShow] = useState(false);
  const [fName, setFname] = useState("");
  const [lName, setLname] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [password, setPassword] = useState("");

  useEffect(() => {
    return () => {
      Keyboard.remove("keyboardDidShow", _keyboardDidShow);
      Keyboard.remove("keyboardDidHide", _keyboardDidHide);
    };
  }, []);

  // useEffect(() => {
  //   if (isSuccess) {
  //     // navigation.pop();
  //     navigation.navigate(AppRoutes.HOME_NAV);
  //     setFname("");
  //     setLname("");
  //     setEmail("");
  //     setPhone("");
  //     setPassword("");
  //   }
  // }, [isSuccess, navigation]);

  const _keyboardDidShow = () => {
    setKeyboardShow(true);
  };

  const _keyboardDidHide = () => {
    setKeyboardShow(false);
  };

  // const onRegister = async () => {
  //   try {
  //     await registerUser({
  //       last_name: lName,
  //       first_name: fName,
  //       email,
  //       phone,
  //       password,
  //     }).unwrap();
  //   } catch (err) {
  //     // console.log("error");
  //   }
  // };

  return (
    <KeyboardAwareScrollView
      showsVerticalScrollIndicator={false}
      style={{}}
      resetScrollToCoords={{ x: 0, y: 0 }}
      contentContainerStyle={{ flex: 1 }}
      scrollEnabled={keyShown}>
      <View style={styles.containerStyles}>
        {/* <ActionButton
          onPress={() => navigation.pop()}
          customStyles={{
            ...styles.closeBtnStyles,
            top: Platform.OS === "ios" ? insets.top - 30 : insets.top + 10,
          }}>
          {CloseIcon}
        </ActionButton> */}
        <Typography variant="h2" customStyles={{ fontSize: 22, top: -20 }}>
          Register to access
        </Typography>
        <Typography variant="h2" customStyles={{ fontSize: 22, top: -10 }}>
          all services
        </Typography>

        <Container
          row
          center
          middle
          customStyles={{
            position: "absolute",
            left: 0,
            right: 0,
            bottom: 20,
            flexWrap: "wrap",
            // backgroundColor: "red",
          }}>
          <Container
            column
            middle
            center
            customStyles={{
              marginRight: 15,
              height: wp(19),
              width: wp(19),
              borderRadius: wp(20 / 3),
              backgroundColor: "rgba(52, 152, 219 ,0.1)",
            }}>
            {BusIcon}
          </Container>
          <Container
            column
            middle
            center
            customStyles={{
              marginRight: 15,
              height: wp(19),
              width: wp(19),
              borderRadius: wp(20 / 3),
              backgroundColor: "rgba(125, 206, 160,0.1)",
            }}>
            {CardIcon}
          </Container>
          <Container
            column
            middle
            center
            customStyles={{
              marginRight: 15,
              height: wp(19),
              width: wp(19),
              borderRadius: wp(20 / 3),
              backgroundColor: "rgba(89, 76, 159,0.1)",
            }}>
            {MotoIcon}
          </Container>

          <Container
            column
            middle
            center
            customStyles={{
              marginRight: 5,
              height: wp(19),
              width: wp(19),
              borderRadius: wp(20 / 3),
              backgroundColor: "rgba(231, 76, 60,0.1)",
            }}>
            {CarIcon}
          </Container>
        </Container>
      </View>

      {/* for container  */}
      <Container
        middle
        flex={0.45}
        customStyles={{ padding: 20 }}
        bgColor={"#F8F9F9"}>
        <Container row center>
          <Input
            placeholder="First name"
            placeholderTextColor={"#7B7D7D"}
            inputWrapperStyles={styles.inputWrapper}
            inputStyles={styles.inputStyles}
            customStyles={{ marginBottom: 10, marginRight: 10, flex: 1 }}
            icon={UserIcon}
          />
          <Input
            placeholder="last name"
            placeholderTextColor={"#7B7D7D"}
            inputWrapperStyles={styles.inputWrapper}
            inputStyles={styles.inputStyles}
            customStyles={{ marginBottom: 10, flex: 1 }}
          />
        </Container>
        <Input
          placeholder="E-mail"
          placeholderTextColor={"#7B7D7D"}
          inputWrapperStyles={styles.inputWrapper}
          inputStyles={styles.inputStyles}
          maxLength={12}
          customStyles={{ marginBottom: 15 }}
          enablesReturnKeyAutomatically
          icon={EmailIcon}
        />
        <Input
          placeholder="Phone"
          placeholderTextColor={"#7B7D7D"}
          inputWrapperStyles={styles.inputWrapper}
          inputStyles={styles.inputStyles}
          maxLength={12}
          keyboardType="numeric"
          customStyles={{ marginBottom: 10 }}
          enablesReturnKeyAutomatically
          icon={PhoneIcon}
        />
        <Input
          placeholder="password"
          placeholderTextColor={"#7B7D7D"}
          returnKeyLabel={"login"}
          returnKeyType="send"
          inputWrapperStyles={styles.inputWrapper}
          inputStyles={styles.inputStyles}
          secure
          customStyles={{ marginBottom: 10 }}
          icon={LockIcon}
        />
      </Container>

      {/* footer */}
      <Container
        flex={0.2}
        space="space-between"
        customStyles={{ paddingHorizontal: 20, paddingBottom: 10 }}
        bgColor={"#F8F9F9"}>
        <PrimaryButton
          // onPress={onRegister}
          // disabled={isLoading}
          customStyles={{
            borderRadius: 4,
            flexDirection: "row",
          }}>
          {/* {isLoading ? (
            <ActivityIndicator size="small" color="#fff" />
          ) : (
            <Typography
              variant="title"
              color="#fff"
              customStyles={{ fontSize: 17 }}>
              Register
            </Typography>
          )} */}
          <Typography
            variant="title"
            color="#fff"
            customStyles={{ fontSize: 17 }}>
            Register
          </Typography>
        </PrimaryButton>

        <Container
          row
          center
          customStyles={{ marginVertical: 20, justifyContent: "flex-end" }}>
          <Typography
            variant="title"
            customStyles={{ fontSize: 17, marginHorizontal: 10 }}>
            Do you have an account ?
          </Typography>
          <ActionButton
            onPress={() => navigation.navigate(AppRoutes.SIGN_IN)}
            customStyles={{
              backgroundColor: "transparent",
            }}>
            <Typography
              variant="title"
              color={"#3498DB"}
              customStyles={{ fontSize: 18 }}>
              Log in
            </Typography>
          </ActionButton>
        </Container>
      </Container>
    </KeyboardAwareScrollView>
  );
};

export default Signup;

const styles = StyleSheet.create({
  containerStyles: {
    width: "100%",
    flex: 0.35,
    flexDirection: "column",
    justifyContent: "center",
    backgroundColor: "#E5E7E9",
    // borderBottomLeftRadius: 75,
    alignItems: "center",
  },
  closeBtnStyles: {
    position: "absolute",
    left: 10,
    height: 40,
    width: 40,
    borderRadius: 40 / 2,
    backgroundColor: "rgba(33, 47, 61,0.5)",
  },
  inputWrapper: {
    backgroundColor: "transparent",
    borderBottomColor: "#313131",
    borderBottomWidth: 0.4,
  },
  inputStyles: {
    fontFamily: "ArialRoundedMTBold",
    color: "#313131",
  },
});
