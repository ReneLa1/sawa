/* eslint-disable quotes */
const actionTypes = {
  //api request
  API_REQUEST: "api_request",

  //login user
  LOGIN_SUCCESS: "login_successful",
  LOGIN_FAIL: "login_failed",

  //sign up user
  SIGNUP_SUCCESS: "sign_up successful",
  SIGNUP_FAIL: "sign_up_fail",

  VERIFY_SUCCESS: "verify success",
  VERIFY_FAIL: "verify fail",

  REQUEST_RESET_SUCCESS: "request reset success",
  REQUEST_RESET_FAIL: "request reset fail",

  RESET_PASSWORD_SUCCESS: "reset password success",
  RESET_PASSWORD_FAIL: "reset password fail",

  //user api
  SAVE_CURRENT_USER: "save_user",

  //user current location
  SAVE_CURRENT_LOCATION: "save_current_location",
  LOCATION_INPUT: "location_input_address",
  SAVE_DESTINATION_LOCATION: "save_destination_location",
  GET_NEAR_RIDERS: "get near riders",

  //driver or moto actions
  SAVE_TRIP_CODE: "save_trip_code",
  FIND_NEAR_DRIVER_FAIL: "find_near_driver_fail",
  SAVE_DRIVER: "save-driver",

  //travel history actions
  REQUEST_HISTORY: "request_history",
  REQUEST_HISTORY_SUCCESS: "request_history_success",
  REQUEST_HISTORY_FAIL: "request_history_fail",

  GET_DATE: "save_date",
  SAVE_RATES: "save_rating",
  //routes direction actions
  SAVE_ROUTE: "save route related data",

  SAVE_LANGUAGE: "save_language",

  //ui action types
  SHOW_SPINNER: "show_spinner",
  HIDE_SPINNER: "hide_spinner",
  LOGIN_IN_PROGRESS: "login_in_progress",
  LOGIN_COMPLETE: "login_complete",
  VERIFY_IN_PROCESS: "verify in process",
  VERIFY_COMPLETE: "verify complete",
  USER_INVALID: "user_invalid",
  SIGNUP_IN_PROGRESS: "sign_up_in_progress",
  SIGN_UP_COMPLETE: "sign_up_complete",
  FETCH_CURRENT_LOCATION_IN_PROCESS: "fetch_current_location_in_process",
  FETCH_CURRENT_LOCATION_COMPLETE: "fetch_current_location_complete",
  FINDING_DRIVER_IN_PROCESS: "finding_driver_in_process",
  FINDING_DRIVER_COMPLETE: "finding_driver_complete",
  DRIVER_FOUND: "driver_found",
  CLEAR_DATA: "clear_data",
  FETCH_IN_PROCESS: "fetching in process",
  FETCH_COMPLETE: "fetching complete",

  SHOW_APP: "show app",
  //fetch bus module ui action types
  FETCH_STATIONS_IN_PROCESS: "fetch_stations_in_process",
  FETCH_STATIONS_COMPLETE: "fetch_stations_complete",
  CHECK_DESTINATION_IN_PROCESS: "check destination in process",
  CHECK_DESTINATION_COMPLETE: "check destination complete",
  BOOKING_IN_PROCESS: "booking ticket in process",
  BOOKING_COMPLETE: "booking complete",
  GET_TICKET_DETAILS_IN_PROCESS: "get ticket details in process",
  GET_TICKET_DETAILS_COMPLETE: "get ticket details complete",

  //card ui related actions
  TOP_IN_PROCESS: "top in process",
  TOP_COMPLETE: "top complete",
  ADD_CARD_IN_PROCESS: "add card in process",
  ADD_CARD_COMPLETE: "add card complete",

  //rating
  RATING_IN_PROGRESS: "rating_in_progress",
  RATING_COMPLETE: "rating_complete",

  //route ui related actions
  DRIVER_ON_WAY: "driver coming to pick up location",
  DRIVER_ARRIVED_PICKUP: "driver arrives at pickup location",
  JOURNEY_START: "journey_start",
  JOURNEY_COMPLETE: "journey_complete",

  FETCHING_HISTORY_IN_PROGRESS: "fetching_history_in_progress",
  FETCHING_HISTORY_COMPLETE: "fetching_history_complete",

  //theme
  SWITCH_THEME: "switch theme",

  //bus upcountry actions
  FETCH_START_STATIONS: "fetch start stations",
  FETCH_STOP_STATIONS: "fetch stop stations",
  CHECK_DESTINATION_SUCCESS: "check destination success",
  CHECK_DESTINATION_FAIL: "check destination fail",
  BOOKING_SUCCESS: "booking successful",
  BOOKING_FAIL: "booking error",

  SAVE_DATE: "save date",
  SAVE_FROM_STATION: "save from station",
  SAVE_TO_STATION: "save to station",
  SAVE_TRIP: "save trip",
  SELECT_TRAVELLER: " select traveller",
  ADD_NEW_TRAVELLER: "add new traveller",

  FETCH_TICKETS: "fetch tickets",
  FETCH_TICKET_DETAILS: "fetch ticket details",
  BOOK_PAYMENT_SUCCESS: "book payment success",
  BOOK_PAYMENT_FAIL: "book payment fail",

  // card related action types
  SAVE_CURRENT_CARD: "save current card",
  GET_CARDS: "get cards",
  CARD_TOP_SUCCESS: "card top success",
  CARD_TOP_FAIL: "card top up fail",

  DELETE_CARD: "delete card",
  GET_CARDS_DETAILS_SUCCESS: "get card details success",
  GET_CARD_DETAILS_FAIL: "get card details fail",
  GET_PAYMENTS: "get payment types",
  GET_CARD_TYPES: "get card types",
  GET_CARD_TRANSACTIONS: "get card transactions",
  GET_ALL_CARD_TRANSACTIONS: "get all card transactions",
  ADD_CARD_SUCCESS: "add card success",
  ADD_CARD_FAIL: "add card fail",
  SAVE_SERVICE: "save service",
};
export default actionTypes;
