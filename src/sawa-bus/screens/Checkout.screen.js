/* eslint-disable react/self-closing-comp */
/* eslint-disable quotes */
import React from "react";
import { StyleSheet, Text } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { ActionButton, Container, Typography } from "../../common";
import GradientBackground from "../components/GradientBackground";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { useNavigation } from "@react-navigation/native";
import { useSelector } from "react-redux";
import AnimatedTotal from "../components/AnimatedTotal";

const Checkout = () => {
  const insets = useSafeAreaInsets();
  const navigation = useNavigation();
  const { selectedTicket } = useSelector(({ Trip }) => Trip);

  const header = (
    <Container
      row
      center
      space="space-between"
      // bgColor="rgba(49, 172, 101,0.9)"
      customStyles={[styles.headerWrapperStyles, { paddingTop: 20 }]}>
      <ActionButton
        onPress={() => navigation.goBack()}
        customStyles={[styles.backBtnStyles]}>
        <FontAwesome name="angle-down" size={24} color="#fff" />
      </ActionButton>

      <Container column center middle>
        <Typography
          customStyles={styles.routeTextStyles}
          variant="title"
          color="#fff">
          {selectedTicket?.ticket.from + " - " + selectedTicket?.ticket.to}
        </Typography>
        <Text style={styles.dateTextStyles} variant="body">
          {selectedTicket?.ticket.date}
        </Text>
      </Container>
      <Typography customStyles={styles.routeTextStyles} variant="title">
        {""}
      </Typography>
    </Container>
  );
  return (
    <Container flex={1} bgColor={"#F2F4F4"}>
      <GradientBackground />
      <Container flex={1} customStyles={styles.contentWrapperStyles}>
        {header}
        <AnimatedTotal ticket={selectedTicket.ticket} />
      </Container>
    </Container>
  );
};

export default Checkout;
const styles = StyleSheet.create({
  containerStyles: {},
  contentWrapperStyles: {
    zIndex: 3,
  },
  headerWrapperStyles: {
    paddingBottom: 15,
    paddingHorizontal: 20,
  },
  backBtnStyles: {},
  routeTextStyles: {
    marginLeft: 10,
    fontWeight: "300",
    fontSize: 18,
  },
  dateTextStyles: {
    marginLeft: 10,
    fontWeight: "600",
    fontSize: 15,
    color: "#fff",
  },
});
