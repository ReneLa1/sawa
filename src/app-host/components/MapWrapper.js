/* eslint-disable quotes */
import React from "react";
import { Dimensions, StyleSheet } from "react-native";
import Animated, {
  useAnimatedStyle,
  useDerivedValue,
  withSpring,
} from "react-native-reanimated";

const { height: SCREEN_HEIGHT } = Dimensions.get("window");
const MAX_TRANSLATE_Y = -SCREEN_HEIGHT + 50;

const MapWrapper = ({ translateY, children }) => {
  const followY = useDerivedValue(() => {
    return withSpring(MAX_TRANSLATE_Y - translateY.value, { damping: 20 });
  });

  const rStyle = useAnimatedStyle(() => {
    return {
      transform: [{ translateY: -followY.value }],
    };
  });

  return (
    <Animated.View style={[rStyle, styles.containerStyles]}>
      {children}
    </Animated.View>
  );
};
export default MapWrapper;
const styles = StyleSheet.create({
  containerStyles: {
    alignItems: "center",
    justifyContent: "center",
    height: SCREEN_HEIGHT / 1.6,
    position: "absolute",
    width: "100%",
    bottom: SCREEN_HEIGHT / 1.2,
  },
});
