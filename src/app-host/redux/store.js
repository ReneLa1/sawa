/* eslint-disable quotes */
import { configureStore } from "@reduxjs/toolkit";
import { apiSlice } from "./api/api.slice";
import utilsReducers from "./reducers/utils.reducers";
import authReducer from "./user/auth.slice";
import serviceReducer from "./services/services.slice";
import logger from "redux-logger";
import tripReducer from "../../sawa-bus/redux/trip.slice.js";

export const store = configureStore({
  reducer: {
    [apiSlice.reducerPath]: apiSlice.reducer,
    auth: authReducer,
    Services: serviceReducer,
    Trip: tripReducer,
    Utils: utilsReducers,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware().concat(apiSlice.middleware).concat(logger),
  devTools: process.env.NODE_ENV !== "production",
});
