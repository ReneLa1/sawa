/* eslint-disable quotes */
import actionTypes from "../actions/types";

const initialState = {
  authUser: null,
  //   userToken: null,
  //   loginError: null,
  //   signError: null,
  //   signupSuccess: false,
  //   verifyError: null,
  //   requesting: false,
  //   errorWhileRequesting: null,
  //   currentLocation: null,
  //   resetSuccess: false,
  //   resetMessage: null,
  //   updated: false,
  //   isUpdating: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        authUser: action.payload.user,
        userToken: action.payload.token,
      };
    // case actionTypes.LOGIN_FAIL:
    //   return {
    //     ...state,
    //     loginError: action.payload,
    //   };
    // case actionTypes.SIGNUP_SUCCESS:
    //   return {
    //     ...state,
    //     // authUser: action.payload.user,
    //     // userToken: action.payload.token,
    //     signupSuccess: true,
    //   };
    // case actionTypes.SIGNUP_FAIL:
    //   return {
    //     ...state,
    //     signError: action.payload,
    //   };
    // case actionTypes.VERIFY_SUCCESS:
    //   return {
    //     ...state,
    //   };
    // case actionTypes.VERIFY_FAIL:
    //   return {
    //     ...state,
    //     verifyError: action.payload,
    //   };
    // case actionTypes.SAVE_CURRENT_USER:
    //   return {
    //     ...state,
    //     authUser: action.payload,
    //     userToken: action.payload.token,
    //   };

    // case actionTypes.USER_LOCATION_REQUEST_SUCCESS:
    //   return {
    //     ...state,
    //     currentLocation: action.payload,
    //   };
    // case "update progressing": {
    //   return {
    //     ...state,
    //     isUpdating: true,
    //   };
    // }
    // case "updated user": {
    //   return {
    //     ...state,
    //     updated: true,
    //     isUpdating: false,
    //   };
    // }
    // case actionTypes.CLEAR_DATA:
    //   return {
    //     ...state,
    //     [action.payload.prop]: action.payload.val,
    //   };
    default:
      return state;
  }
};
