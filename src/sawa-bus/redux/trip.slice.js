/* eslint-disable dot-notation */
/* eslint-disable quotes */
import { createSlice } from "@reduxjs/toolkit";
import { apiSlice } from "../../app-host/redux/api/api.slice";
import { format } from "date-fns";

const initialState = {
  trip: {
    from: "KIGALI",
    to: null,
    date: null,
  },
  recentSearches: [],
  selectedTicket: {
    ticket: null,
    payment: null,
    contact: null,
  },
};

const tripSlice = createSlice({
  name: "Trip",
  initialState,
  reducers: {
    setDepartStation: ({ trip }, { payload }) => {
      trip["from"] = payload;
    },
    setDestStation: ({ trip }, { payload }) => {
      trip["to"] = payload;
    },
    addToHistory: ({ recentSearches }, { payload }) => {
      recentSearches = [payload, ...recentSearches];
    },
    chooseDate: ({ trip }, { payload }) => {
      trip["date"] = payload;
    },
    onChooseTicket: ({ selectedTicket }, { payload }) => {
      selectedTicket[payload.prop] = payload.value;
    },
  },
});

export const tripData = apiSlice.injectEndpoints({
  endpoints: build => ({
    getAvailableBuses: build.mutation({
      query: tripInfo => ({
        url: "/mobile/check-destination",
        method: "POST",
        body: {
          travel_date: format(tripInfo.date, "dd-MM-yyyy"),
          travel_time: format(tripInfo.date, "hh:mm"),
          start_point: tripInfo.from,
          end_point: tripInfo.to,
        },
        headers: {
          os: "android",
        },
      }),
      invalidatesTags: [{ type: "TRIP_DATA", id: "BUS_LIST" }],
    }),
  }),
});

export const {
  setDepartStation,
  setDestStation,
  addToHistory,
  chooseDate,
  onChooseTicket,
} = tripSlice.actions;
export const { useGetAvailableBusesMutation } = tripData;

export default tripSlice.reducer;
