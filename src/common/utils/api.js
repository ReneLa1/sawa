import {
  MOTO_BASE_URL,
  ROOT_BASE_URL,
  TEST_BASE_URL,
  GOOGLE_MAP_API_KEY,
} from "@env";

export const motoUrl = MOTO_BASE_URL;
export const rootUrl = ROOT_BASE_URL;
export const testUrl = TEST_BASE_URL;

export const mapAPI_key = GOOGLE_MAP_API_KEY;
