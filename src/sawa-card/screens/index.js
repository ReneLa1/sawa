/* eslint-disable quotes */
/* eslint-disable react-native/no-inline-styles */
import React from "react";

import { View, Text } from "react-native";

const SawaCard = () => {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text style={{ color: "red", fontSize: 15 }}>
        Tap&Go Chunk downloaded from remote server
      </Text>
    </View>
  );
};

export default SawaCard;
