// /* eslint-disable quotes */
// /* eslint-disable no-unused-vars */
// /* eslint-disable no-undef */
// import React, { createContext, useEffect } from "react";
// import firebaseConfig from "./firebaseConfig";
// import app from "firebase/app";
// import "firebase/database";

// // import { useDispatch } from "react-redux";

// //Creating a React Context, fr this to be accessible
// //form the components later

// const FirebaseContext = createContext(null);
// export { FirebaseContext };

// export default ({}) => {
//   let firebase = {
//     app: null,
//     database: null,
//   };
//   //   const dispatch = useDispatch();

//   // check if firebase app has been initialized previously
//   // if not, initialize with the config we saved earlier
//   if (!app.apps.length) {
//     app.initializeApp(firebaseConfig);
//     firebase = {
//       app: app,
//       database: app.database(),

//       //   api: {
//       //     getServices,
//       //   },
//     };
//   }

//   const getServices = () => {
//     firebase.database.ref("todos").on("value", snapshot => {
//       const vals = snapshot.val();
//       let _records = [];
//       for (var key in vals) {
//         _records.push({
//           ...vals[key],
//           id: key,
//         });
//       }
//       // setServices is a Redux action that would update the todo store
//       // to the _records payload
//     });
//   };

//   return (
//     <FirebaseContext.Provider value={firebase}>
//       {children}
//     </FirebaseContext.Provider>
//   );
// };
