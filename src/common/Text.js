import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {useSelector} from 'react-redux';

const Typography = props => {
  const {theme} = useSelector(({Utils}) => Utils);
  const {
    variant,
    size,
    transform,
    align,
    spacing, // letter-spacing
    customStyles,
    children,
    color,
    className,
    ...other
  } = props;
  const textColor = color ? color : theme.PRIMARY_TEXT_COLOR;
  const textStyles = [
    styles.text,
    variant === 'h1' && styles.h1,
    variant === 'h2' && styles.h2,
    variant === 'title' && styles.title,
    variant === 'body' && styles.body,
    variant === 'caption' && styles.caption,
    variant === 'caption2' && styles.caption2,
    textColor && {color: textColor},
    customStyles,
  ];

  return (
    <Text className={className} style={[textStyles]} {...other}>
      {children}
    </Text>
  );
};

export default Typography;

const styles = StyleSheet.create({
  // default style
  text: {
    fontFamily: 'ArialRoundedMTBold',
    fontSize: 18,
    letterSpacing: 0.25,
  },
  // variations
  h1: {
    fontFamily: 'ArialRoundedMTBold',
    fontSize: 28,
    letterSpacing: 0,
  },
  h2: {
    fontFamily: 'ArialRoundedMTBold',
    fontSize: 24,
    fontWeight: '400',
    letterSpacing: 0.15,
  },

  title: {
    fontFamily: 'ArialRoundedMTBold',
    fontSize: 20,
    letterSpacing: -1,
  },
  body: {
    fontFamily: 'ArialRoundedMTBold',
    fontSize: 16,
    letterSpacing: -1,
  },
  caption: {
    fontFamily: 'ArialRoundedMTBold',
    fontSize: 13,
    letterSpacing: 0.19,
  },
  caption2: {
    fontFamily: 'ArialRoundedMTBold',
    fontSize: 13,
    letterSpacing: 0.19,
  },
});
