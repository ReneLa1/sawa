import { ChunkManager } from "@callstack/repack/client";
import { useNavigation, useRoute } from "@react-navigation/native";
import React from "react";
import { Dimensions, StyleSheet } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import AntDesign from "react-native-vector-icons/AntDesign";
import Feather from "react-native-vector-icons/Feather";
import { ActionButton, Container } from "../../../common";
import PageLoader from "../../../common/PageLoader";

import { createStackNavigator } from "@react-navigation/native-stack";
import { BusStack } from "../../../sawa-bus/navigation/BusStack";

ChunkManager.configure({
  resolveRemoteChunk: async chunkId => {
    return {
      url: `https://res.cloudinary.com/dsubmyluz/image/upload/v1654175206/Sawa/${chunkId}`,
    };
  },
});

const { height } = Dimensions.get("window");

const SawaBusModule = React.lazy(() => {
  return Promise.all([
    import(/* webpackChunkName: "SawaBus" */ "../../../sawa-bus/screens"),
    new Promise(resolve =>
      setTimeout(resolve, Math.floor(Math.random() * (1000 - 400 + 1)) + 400),
    ),
  ]).then(([moduleExports]) => moduleExports);
});
const SawaCardModule = React.lazy(() => {
  return Promise.all([
    import(/* webpackChunkName: "SawaCard" */ "../../../sawa-card/screens"),
    new Promise(resolve =>
      setTimeout(resolve, Math.floor(Math.random() * (1000 - 400 + 1)) + 400),
    ),
  ]).then(([moduleExports]) => moduleExports);
});

const CloseBtn = ({}) => {
  const navigation = useNavigation();
  return (
    <Container
      row
      center
      space="space-around"
      customStyles={styles.closeBtnStyles}>
      <ActionButton>
        <Feather name="more-horizontal" size={20} color="black" />
      </ActionButton>
      <ActionButton onPress={() => navigation.goBack()}>
        <AntDesign name="closecircleo" size={20} color="black" />
      </ActionButton>
    </Container>
  );
};

const SawaBusScreen = () => {
  const insets = useSafeAreaInsets();
  const {
    params: { service },
  } = useRoute();
  return (
    <React.Suspense
      fallback={
        <Container flex={1} center middle customStyles={styles.loaderContainer}>
          <Container
            row
            customStyles={{
              position: "absolute",
              top: insets.top,
              right: 15,
            }}>
            <CloseBtn />
          </Container>
          <PageLoader />
        </Container>
      }>
      <SawaBusModule title={service.title} />
    </React.Suspense>
  );
};

const SawaCardScreen = () => {
  const insets = useSafeAreaInsets();
  const {
    params: { service },
  } = useRoute();
  return (
    <React.Suspense
      fallback={
        <Container flex={1} center middle customStyles={styles.loaderContainer}>
          <Container
            row
            customStyles={{
              position: "absolute",
              top: insets.top,
              right: 15,
            }}>
            <CloseBtn />
          </Container>
          <PageLoader />
        </Container>
      }>
      <SawaCardModule title={service.title} />
    </React.Suspense>
  );
};

const Stack = createStackNavigator();

const ModuleNavigator = () => {
  const {
    params: { service },
  } = useRoute();
  return (
    <Stack.Navigator>
      {service.name === "SawaBus" ? (
        <Stack.Screen name={"SAWA BUS STACK"} component={BusStack} />
      ) : (
        <Stack.Screen name={"SAWA CARD STACK"} component={SawaCardScreen} />
      )}
    </Stack.Navigator>
  );
};

const AppContainer = () => {
  const insets = useSafeAreaInsets();
  const {
    params: { service },
  } = useRoute();

  // console.log(service);
  if (service.name === "SawaBus") {
    return (
      <React.Suspense
        fallback={
          <Container
            flex={1}
            center
            middle
            customStyles={styles.loaderContainer}>
            <Container
              row
              customStyles={{
                position: "absolute",
                top: insets.top,
                right: 15,
              }}>
              <CloseBtn />
            </Container>
            <PageLoader />
          </Container>
        }>
        <SawaBusModule title={service.title} />
      </React.Suspense>
    );
  }
  return (
    <React.Suspense
      fallback={
        <Container flex={1} center middle customStyles={styles.loaderContainer}>
          <Container
            row
            center
            customStyles={{
              position: "absolute",
              top: insets.top + 5,
              right: 15,
            }}>
            <CloseBtn />
          </Container>
          <PageLoader />
        </Container>
      }>
      <SawaCardModule title={service.title} />
    </React.Suspense>
  );
};

export default AppContainer;

const styles = StyleSheet.create({
  loaderContainer: {
    backgroundColor: "#fff",
    height: "100%",
    width: "100%",
  },
  closeBtnStyles: {
    height: 30,
    backgroundColor: "#fff",
    paddingHorizontal: 4,
    paddingVertical: 4,
    borderRadius: 20,
  },
  linearGradientBackdrop: {
    position: "absolute",
    height: height / 2.7,
    top: 0,
    left: 0,
    right: 0,
    zIndex: 0,
  },
});
