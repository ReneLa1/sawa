/* eslint-disable quotes */
import { createSlice, createEntityAdapter } from "@reduxjs/toolkit";

const servicesAdapter = createEntityAdapter({
  sortComparer: (a, b) => a.name.localeCompare(b.name),
});

const initialState = servicesAdapter.getInitialState({
  services: null,
});

const serviceSlice = createSlice({
  name: "Services",
  initialState,
  reducers: {
    saveServices: (state, { payload }) => {
      state.services = payload;
    },
  },
});

export const {
  selectAll: selectAllServices,
  selectById: selectServiceById,
  selectIds: selectServiceIds,
} = servicesAdapter.getSelectors(state => state.Services);

export const { saveServices } = serviceSlice.actions;

export default serviceSlice.reducer;
