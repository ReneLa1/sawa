/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable quotes */
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useGetAvailableBusesMutation } from "../redux/trip.slice";
import { ActionButton, Container, Typography } from "../../common";
import GradientBackground from "../components/GradientBackground";
import { FlatList, Image, StyleSheet } from "react-native";
import Feather from "react-native-vector-icons/Feather";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import TicketLoader from "../components/TicketLoader";
import BusTicket from "../components/BusTicket";
import Animated, { FadeInDown, ZoomIn } from "react-native-reanimated";
import { useNavigation } from "@react-navigation/native";
import { format } from "date-fns";

const FetchIndicator = () => {
  return (
    <Animated.View
      entering={FadeInDown.springify().delay(1)}
      style={{
        flex: 1,
        flexDirection: "column",
        paddingHorizontal: 20,
        paddingTop: 30,
      }}>
      <Container>
        <TicketLoader />
        <TicketLoader />
        <TicketLoader />
      </Container>
    </Animated.View>
  );
};

const EmptyResults = ({ message }) => {
  return (
    <Animated.View
      entering={ZoomIn.springify().delay(5)}
      style={{
        paddingHorizontal: 20,
        paddingTop: 30,
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        flex: 0.5,
      }}>
      <Image
        source={require("../../../assets/icons/no-results.png")}
        style={{ width: 50, height: 50, marginBottom: 10 }}
        resizeMode="contain"
      />
      <Typography variant="h1" customStyles={{ fontSize: 16 }}>
        {message}
      </Typography>
    </Animated.View>
  );
};
const ScheduledBuses = () => {
  const insets = useSafeAreaInsets();
  const navigation = useNavigation();
  const { trip } = useSelector(state => state.Trip);
  const [getAvailableBuses, { data, error, isLoading, isSuccess }] =
    useGetAvailableBusesMutation();

  useEffect(() => {
    getAvailableBuses(trip);
  }, []);

  const header = (
    <Container
      row
      center
      space="space-between"
      customStyles={{
        paddingTop: insets.top + 5,
        width: "100%",
        paddingHorizontal: 18,
        marginBottom: 15,
      }}>
      <ActionButton
        onPress={() => navigation.goBack()}
        customStyles={styles.searchBtnStyles}
        activeScale={0.95}
        tension={50}
        friction={7}
        useNativeDriver>
        <Feather name="search" size={20} color="rgb(202,204,208)" />
        <Typography customStyles={styles.routeTextStyles} variant="title">
          {`${trip.from} - ${trip.to}`}
        </Typography>
        <Typography customStyles={styles.dateTextStyles} variant="body">
          {format(trip.date, "dd-MM-yyyy")}
        </Typography>
      </ActionButton>
    </Container>
  );

  return (
    <Container flex={1} customStyles={styles.wrapperStyles}>
      <GradientBackground />
      <Container flex={1} customStyles={{ zIndex: 3 }}>
        {header}
        {isLoading ? (
          <FetchIndicator />
        ) : isSuccess ? (
          <FlatList
            showsVerticalScrollIndicator={false}
            data={data?.data}
            style={{ flex: 1 }}
            contentContainerStyle={{ paddingHorizontal: 15, paddingTop: 30 }}
            keyExtractor={i => i}
            renderItem={({ item }) => {
              return <BusTicket bus={item} />;
            }}
          />
        ) : error ? (
          <EmptyResults message={error?.data.message} />
        ) : (
          <Container />
        )}
      </Container>
    </Container>
  );
};
export default ScheduledBuses;

const styles = StyleSheet.create({
  wrapperStyles: {
    backgroundColor: "#F2F4F4",
  },
  searchBtnStyles: {
    flexDirection: "row",
    justifyContent: "flex-start",
    backgroundColor: "#fff",
    borderRadius: 5,
    flex: 1,
    marginRight: 5,
    height: 45,
    paddingLeft: 10,
    paddingRight: 10,
  },
  routeTextStyles: {
    marginLeft: 10,
    fontWeight: "300",
    fontSize: 16,
  },
  dateTextStyles: {
    marginLeft: 10,
    fontWeight: "300",
    fontSize: 16,
    color: "rgba(156,156,156,255)",
  },
});
