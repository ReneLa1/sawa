/* eslint-disable react-native/no-inline-styles */
/* eslint-disable quotes */
import { useNavigation } from "@react-navigation/native";
import React, { useEffect } from "react";
import {
  Dimensions,
  Image,
  ImageBackground,
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from "react-native";
import AppIntroSlider from "react-native-app-intro-slider";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { useSelector } from "react-redux";
import { Container } from "../../../common";
import { AppRoutes } from "../../navigation/app.routes";
import { useAuth } from "../../hooks/useAuth";

const cloudImg = require("../../../../assets/images/cloud.png");

const { height, width } = Dimensions.get("screen");
const slides = [
  {
    key: 1,
    text: "Get live updates",
    desc: "Your Bus is arriving Now",
    image: require("../../../../assets/images/Group-4.png"),
  },
  {
    key: 2,
    text: "Affordable Rides You Can Count On",
    desc: "Hire / Rent",
    image: require("../../../../assets/images/Group-5.png"),
  },
  {
    key: 3,
    text: "Book Intercity Bus Tickets Instantly",
    desc: "Top up you Tap%Go Card",
    image: require("../../../../assets/images/cards.png"),
  },
  {
    key: 4,
    text: "Let's Make your Trip even better",
    desc: "",
    image: require("../../../../assets/images/bus-tick.png"),
  },
];

const AppIntro = () => {
  const { user } = useAuth();

  const navigation = useNavigation();
  useEffect(() => {
    if (Platform.OS === "ios") {
      StatusBar.setBarStyle("dark-content");
    }
  }, []);
  const renderItem = ({ item }) => {
    return item.key === 1 ? (
      <SliderOne item={item} />
    ) : item.key === 2 ? (
      <SliderTwo item={item} />
    ) : item.key === 3 ? (
      <SliderThree item={item} />
    ) : (
      <SliderFour item={item} />
    );
  };
  const _renderButton = txt => {
    return (
      <View style={styles.buttonCircle}>
        <Text style={{ color: "#000", fontWeight: "600", fontSize: 16 }}>
          {txt}
        </Text>
      </View>
    );
  };

  const handleDone = () => {
    user
      ? navigation.navigate(AppRoutes.HOME_NAV)
      : navigation.navigate(AppRoutes.AUTH_STACK);
  };
  return (
    <AppIntroSlider
      renderItem={renderItem}
      data={slides}
      onDone={() => handleDone()}
      showSkipButton
      activeDotStyle={{ backgroundColor: "#1A459C" }}
      renderNextButton={() => _renderButton("Next")}
      renderDoneButton={() => _renderButton("Done")}
      renderSkipButton={() => _renderButton("Skip")}
    />
  );
};

const SliderOne = ({ item }) => {
  return (
    <Container bgColor="#F2F8FF" flex={1}>
      <Container
        center
        customStyles={{
          width,
          height: hp(45),
          justifyContent: "flex-end",
          paddingBottom: 10,
        }}>
        <Image source={item.image} style={{ flex: 1, height: "100%", width }} />
      </Container>

      <ImageBackground
        source={cloudImg}
        style={{
          position: "absolute",
          right: 10,
          top: hp(34),
          width: 240,
          height: 180,
          zIndex: 100,
          alignItems: "center",
          justifyContent: "center",
        }}>
        <Text style={styles.baseText}>
          <Text style={styles.titleText}>
            Your Bus
            {"\n"}
          </Text>
          <Text numberOfLines={5}>
            Is Arriving
            {"\n"}
          </Text>
          <Text numberOfLines={5}>Now</Text>
        </Text>
      </ImageBackground>
      <Container
        // bgColor="red"
        flex={1}
        middle
        center
        customStyles={{
          position: "absolute",
          height: hp(30),
          left: 0,
          right: 0,
          bottom: hp(15),
        }}>
        <Text
          style={{
            fontFamily: "ArialRoundedMTBold",
            fontSize: 30,
            fontWeight: "700",
          }}>
          Get live Update
        </Text>
      </Container>
    </Container>
  );
};

const SliderTwo = ({ item }) => {
  return (
    <Container bgColor="#F2F8FF" flex={1}>
      <Container
        center
        customStyles={{
          width,
          height: hp(45),
          paddingTop: hp(13),
          alignItems: "flex-end",
          paddingBottom: 10,
        }}>
        <ImageBackground
          source={cloudImg}
          style={{
            width: 310,
            height: 230,
            zIndex: 100,
            alignItems: "center",
            justifyContent: "center",
          }}>
          <Text style={[styles.titleText, { marginBottom: 7, fontSize: 30 }]}>
            Affordable
          </Text>
          <Text style={[styles.titleText, { marginBottom: 7, fontSize: 30 }]}>
            Rides You
          </Text>
          <Text style={[styles.titleText, { marginBottom: 7, fontSize: 30 }]}>
            Can Count on.
          </Text>
          <Text style={[styles.baseText, { marginTop: 20, fontSize: 20 }]}>
            Hire/Rent
          </Text>
        </ImageBackground>
      </Container>

      <Container
        // bgColor="green"
        column
        center
        customStyles={{ height: hp(30) }}>
        <Image
          source={item.image}
          style={{ flex: 1, height: "100%", width: wp(80) }}
          resizeMode="contain"
        />
      </Container>
    </Container>
  );
};

const SliderThree = ({ item }) => {
  return (
    <Container bgColor="#F2F8FF" flex={1}>
      <Container
        center
        customStyles={{
          width,
          height: hp(45),
          paddingTop: hp(10),
          alignItems: "flex-end",
          paddingBottom: 10,
        }}>
        <ImageBackground
          source={cloudImg}
          style={{
            width: 310,
            height: 230,
            zIndex: 100,
            alignItems: "center",
            justifyContent: "center",
          }}>
          <Text style={[styles.titleText, { marginBottom: 7, fontSize: 30 }]}>
            Book
          </Text>
          <Text style={[styles.titleText, { marginBottom: 7, fontSize: 30 }]}>
            Intercity
          </Text>
          <Text style={[styles.titleText, { marginBottom: 7, fontSize: 30 }]}>
            Bus Tickets
          </Text>
          <Text style={[styles.titleText, { marginBottom: 7, fontSize: 30 }]}>
            instantly
          </Text>
        </ImageBackground>
      </Container>

      <Container column center customStyles={{ height: hp(30) }}>
        <Image
          source={item.image}
          style={{ height: "100%", width: "100%" }}
          resizeMode="center"
        />
        <Text
          style={{
            fontFamily: "ArialRoundedMTBold",
            fontSize: 24,
            fontWeight: "700",
          }}>
          Top up your Tap&Go
        </Text>
        <Text
          style={{
            fontFamily: "ArialRoundedMTBold",
            fontSize: 23,
            fontWeight: "700",
          }}>
          Card
        </Text>
      </Container>
    </Container>
  );
};

const SliderFour = ({ item }) => {
  return (
    <Container bgColor="#F2F8FF" flex={1}>
      <Container
        center
        customStyles={{
          width,
          height: hp(45),
          paddingTop: hp(13),
          alignItems: "flex-end",
          paddingBottom: 10,
        }}>
        <ImageBackground
          source={cloudImg}
          style={{
            width: 280,
            height: 210,
            zIndex: 100,
            alignItems: "center",
            justifyContent: "center",
          }}>
          <Text style={[styles.titleText, { marginBottom: 7, fontSize: 30 }]}>
            Let's make
          </Text>
          <Text style={[styles.titleText, { marginBottom: 7, fontSize: 30 }]}>
            your trip
          </Text>
          <Text style={[styles.titleText, { marginBottom: 7, fontSize: 30 }]}>
            even better
          </Text>
        </ImageBackground>
      </Container>

      <Container
        // bgColor="green"
        column
        center
        customStyles={{ height: hp(35), paddingLeft: 30, paddingTop: 40 }}>
        <Image
          source={item.image}
          style={{
            height: "100%",
            width: "100%",
            transform: [{ scale: 0.5 }],
          }}
          resizeMode="contain"
        />
      </Container>
    </Container>
  );
};
export default AppIntro;

const styles = StyleSheet.create({
  baseText: {
    fontFamily: "ArialRoundedMTBold",
    fontSize: 20,
    fontWeight: "600",
    // color: "red",
  },
  titleText: {
    // color: "#000",
    fontSize: 30,
    fontWeight: "700",
    fontFamily: "ArialRoundedMTBold",
  },
  scroller: {
    flex: 1,
  },
  buttonCircle: {
    width: 44,
    height: 44,
    // backgroundColor: "rgba(0, 0, 0, .2)",
    // borderRadius: 22,
    justifyContent: "center",
    alignItems: "center",
  },
});
