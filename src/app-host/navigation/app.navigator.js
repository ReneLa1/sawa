/* eslint-disable react/jsx-no-comment-textnodes */
/* eslint-disable prettier/prettier */
/* eslint-disable quotes */
import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { useSelector } from "react-redux";
import { BusStack } from "../../sawa-bus/navigation/BusStack";
import { ModuleNavigator } from "./module.navigator";
import Home from "../screens/home";
import AppIntro from "../screens/intros/Intro.slider";
import { AppRoutes } from "./app.routes";
import { AuthStack } from "./auth.stack";

const Stack = createStackNavigator();

export const AppNavigator = () => {
  const { theme } = useSelector(({ Utils }) => Utils);

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        cardStyle: {
          backgroundColor: theme.SECONDARY_BACKGROUND_COLOR,
        },
      }}>
      <Stack.Group
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name={AppRoutes.INTRO_SLIDER} component={AppIntro} />
      </Stack.Group>

      <Stack.Group screenOptions={{ presentation: "modal" }}>
        <Stack.Screen
          name={AppRoutes.AUTH_STACK}
          component={AuthStack}
          options={({}) => ({
            headerShown: false,
          })}
        />
      </Stack.Group>

      <Stack.Group
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name={AppRoutes.HOME_NAV} component={Home} />
        <Stack.Screen
          name={AppRoutes.DYNAMIC_SCREEN}
          component={ModuleNavigator}
        />
      </Stack.Group>
    </Stack.Navigator>
  );
};
