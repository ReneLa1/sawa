import actionTypes from "../actions/types";

const initialState = {
  pending: false,
  loginInProcess: false,
  registerInProcess: false,
  inValidUser: false,
  verifyInProcess: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SHOW_SPINNER:
      return {
        ...state,
        pending: true,
      };
    case actionTypes.HIDE_SPINNER:
      return {
        ...state,
        pending: false,
      };
    case actionTypes.LOGIN_IN_PROGRESS:
      return {
        ...state,
        loginInProcess: true,
      };
    case actionTypes.LOGIN_COMPLETE:
      return {
        ...state,
        loginInProcess: false,
      };

    case actionTypes.SIGNUP_IN_PROGRESS:
      return {
        ...state,
        registerInProcess: true,
      };
    case actionTypes.SIGN_UP_COMPLETE:
      return {
        ...state,
        registerInProcess: false,
      };
    case actionTypes.USER_INVALID:
      return {
        ...state,
        inValidUser: true,
      };

    case actionTypes.CLEAR_DATA:
      return {
        ...state,
        [action.payload.prop]: action.payload.val,
      };
    default:
      return state;
  }
};
