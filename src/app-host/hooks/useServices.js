import { useEffect, useState } from "react";

import firestore from "@react-native-firebase/firestore";

export default function useServices() {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(null);

  useEffect(() => {
    (async function () {
      try {
        setLoading(true);
        await firestore()
          .collection("services")
          .get()
          .then(querySnapshot => {
            console.log("Total services: ", querySnapshot.size);

            querySnapshot.forEach(documentSnapshot => {
              console.log(
                "service ID: ",
                documentSnapshot.id,
                documentSnapshot.data(),
              );
            });
          });
      } catch (err) {
        setError(err);
      } finally {
        setLoading(false);
      }
    });
  }, []);

  return { data, error, loading };
}
