/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable quotes */
import React, {
  useCallback,
  useEffect,
  forwardRef,
  useImperativeHandle,
} from "react";
import { Dimensions, StyleSheet, Text } from "react-native";
import { Gesture, GestureDetector } from "react-native-gesture-handler";
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withSpring,
} from "react-native-reanimated";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { ActionButton } from "../../common";
import LocationSearch from "./LocationSearch";
import Services from "./Services";

import FontAwesome from "react-native-vector-icons/FontAwesome";
const AngleIcon = <FontAwesome name="angle-right" size={20} color="#fff" />;
const { height: SCREEN_HEIGHT } = Dimensions.get("window");

const ServicesWrapper = ({ translateY, context, gesture, onPress }, ref) => {
  const active = useSharedValue(false);
  const scrollTo = useCallback(pos => {
    "worklet";

    active.value = pos !== 0;
    translateY.value = withSpring(pos, { damping: 50 });
  }, []);
  const isActive = useCallback(() => {
    return active.value;
  }, []);
  useImperativeHandle(ref, () => ({ scrollTo, isActive }), [scrollTo]);
  useEffect(() => {
    scrollTo(-SCREEN_HEIGHT / 2);
  }, []);
  const animContainerStyle = useAnimatedStyle(() => {
    return { transform: [{ translateY: translateY.value }] };
  });
  return (
    <GestureDetector gesture={gesture}>
      <Animated.View style={[animContainerStyle, styles.containerStyles]}>
        <LocationSearch />
        <Services />
        <ActionButton
          activeScale={0.95}
          tension={50}
          friction={7}
          useNativeDriver
          customStyles={styles.offersBtnStyles}>
          <Text style={styles.offerTxtStyles}>Today's Offers & Discounts</Text>
          {AngleIcon}
        </ActionButton>
      </Animated.View>
    </GestureDetector>
  );
};
export default forwardRef(ServicesWrapper);

const styles = StyleSheet.create({
  containerStyles: {
    backgroundColor: "#1A459C",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    height: SCREEN_HEIGHT,
    position: "absolute",
    width: "100%",
    top: SCREEN_HEIGHT,
  },
  offersBtnStyles: {
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "rgba(255, 255, 255, 0.08)",
    marginHorizontal: 20,
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 20,
    paddingRight: 20,
    borderRadius: 5,
    marginTop: hp(3),
  },
  offerTxtStyles: {
    fontSize: 14,
    color: "#fff",
    fontWeight: "600",
    fontFamily: "ArialRoundedMTBold",
  },
});
