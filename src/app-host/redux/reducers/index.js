/* eslint-disable quotes */
/* eslint-disable prettier/prettier */

import { combineReducers } from "redux";
import utilsReducer from "./utils.reducers";
import userReducers from "./user.reducers";
import uiReducers from "./ui.reducers";

export default combineReducers({
  Utils: utilsReducer,
  User: userReducers,
  IU: uiReducers,
});
