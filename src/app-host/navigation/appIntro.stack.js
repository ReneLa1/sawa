/* eslint-disable react/self-closing-comp */
/* eslint-disable prettier/prettier */
/* eslint-disable quotes */
import {
  CardStyleInterpolators,
  createStackNavigator,
} from "@react-navigation/stack";
import React from "react";
import { AppRoutes } from "./app.routes";
import AppIntro from "../screens/intros/Intro.slider";
export const Stack = createStackNavigator();

export const IntroStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <Stack.Screen name={AppRoutes.INTRO_SLIDER} component={AppIntro} />
    </Stack.Navigator>
  );
};
