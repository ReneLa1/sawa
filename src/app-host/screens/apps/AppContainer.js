/* eslint-disable react-native/no-inline-styles */
/* eslint-disable quotes */
// import * as React from "react";
// import { ScrollView, View, Text, Button } from "react-native";
// import { ChunkManager } from "@callstack/repack/client";

// ChunkManager.configure({
//   resolveRemoteChunk: async chunkId => {
//     return {
//       url: `https://res.cloudinary.com/dsubmyluz/image/upload/v1654175206/Sawa/${chunkId}`,
//     };
//   },
// });

// const apps = [
//   {
//     id: "english-module", // Notice: `id` is the same as `webpackChunkName` magic comment
//     title: "Learn English",
//     load: () =>
//       import(/* webpackChunkName: "english-module" */ "./english-module.js"),
//   },
//   {
//     id: "spanish-module",
//     title: "Learn Snapish",
//     load: () =>
//       import(/* webpackChunkName: "spanish-module" */ "./spanish-module.js"),
//   },
// ];

// const AppsContext = React.createContext();

// function AppsProvider({ children }) {
//   const [installedApps, setInstalledApps] = React.useState([]);
//   const [exports, setExports] = React.useState({});

//   const installApp = React.useCallback(async appId => {
//     const app = apps.find(app => app.id === appId);
//     if (app) {
//       const appExports = await app.load();
//       setInstalledApps(installedApps => [...installedApps, appId]);
//       setExports(exports => ({ ...exports, [appId]: appExports }));
//     }
//   }, []);

//   const uninstallApp = React.useCallback(appId => {
//     setInstalledApps(installedApps =>
//       installedApps.filter(installedApp => installedApp.id !== appId),
//     );
//     setExports(exports => ({ ...exports, [appId]: undefined }));
//     ChunkManager.invalidateChunks(appId);
//   }, []); AIzaSyC3Mid3eMVLIA7t0GOii-ZeJ2OWhdlFtNc

//   return ( AIzaSyDmq2kABzJMWf1QqaNOX4Qw49CXYNzHI04
//     <AppsContext.Provider
//       value={{
//         installApp,
//         uninstallApp,
//         installedApps,
//         exports,
//       }}>
//       {children}
//     </AppsContext.Provider>
//   );
// }

// // Somewhere
// function LanguageStore() {
//   const { installedApps, installApp, uninstallApp } =
//     React.useContext(AppsContext);

//   return (
//     <ScrollView>
//       {apps.map(app => (
//         <View>
//           <Text>{app.title}</Text>
//           {installedApps.includes(app.id) ? (
//             <Button
//               onPress={() => uninstallApp(app.id)}
//               title="Remove module"
//             />
//           ) : (
//             <Button onPress={() => installApp(app.id)} title="Add module" />
//           )}
//         </View>
//       ))}
//     </ScrollView>
//   );
// }

// // And then
// function AppRenderer({ appId }) {
//   const { exports } = React.useContext(AppsContext);
//   // You can customize what should happen if exports for the app are not found.
//   const Component = exports[appId]?.RootComponent ?? (() => null);
//Math.floor(Math.random() * (1000 - 500 + 1)) + 500

//   return <Component />;
// }

import { ChunkManager } from "@callstack/repack/client";
import { useNavigation, useRoute } from "@react-navigation/native";
import React from "react";
import { Dimensions, StyleSheet } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import AntDesign from "react-native-vector-icons/AntDesign";
import Feather from "react-native-vector-icons/Feather";
import { ActionButton, Container } from "../../../common";
import PageLoader from "../../../common/PageLoader";

import { createStackNavigator } from "@react-navigation/stack";

ChunkManager.configure({
  resolveRemoteChunk: async chunkId => {
    return {
      url: `https://res.cloudinary.com/dsubmyluz/image/upload/v1654175206/Sawa/${chunkId}`,
    };
  },
});

const { height } = Dimensions.get("window");

const SawaBusModule = React.lazy(() => {
  return Promise.all([
    import(
      /* webpackChunkName: "SawaBus" */ "../../../sawa-bus/navigation/BusStack"
    ),
    new Promise(resolve =>
      setTimeout(resolve, Math.floor(Math.random() * (1000 - 400 + 1)) + 400),
    ),
  ]).then(([moduleExports]) => moduleExports);
});

const CloseBtn = ({}) => {
  const navigation = useNavigation();
  return (
    <Container
      row
      center
      space="space-around"
      customStyles={styles.closeBtnStyles}>
      <ActionButton>
        <Feather name="more-horizontal" size={20} color="black" />
      </ActionButton>
      <ActionButton onPress={() => navigation.goBack()}>
        <AntDesign name="closecircleo" size={20} color="black" />
      </ActionButton>
    </Container>
  );
};

const SawaBusStack = () => {
  const insets = useSafeAreaInsets();
  const {
    params: { service },
  } = useRoute();
  return (
    <React.Suspense
      fallback={
        <Container flex={1} center middle customStyles={styles.loaderContainer}>
          <Container
            row
            customStyles={{
              position: "absolute",
              top: insets.top,
              right: 15,
            }}>
            <CloseBtn />
          </Container>
          <PageLoader />
        </Container>
      }>
      <SawaBusModule service={service} />
    </React.Suspense>
  );
};

const Stack = createStackNavigator();

const ModuleNavigator = () => {
  const {
    params: { service },
  } = useRoute();
  return (
    <Stack.Navigator>
      {service.name === "SawaBus" ? (
        <Stack.Screen name={"SAWA BUS STACK"} component={SawaBusStack} />
      ) : null}
    </Stack.Navigator>
  );
};

export default ModuleNavigator;

const styles = StyleSheet.create({
  loaderContainer: {
    backgroundColor: "#fff",
    height: "100%",
    width: "100%",
  },
  closeBtnStyles: {
    height: 30,
    backgroundColor: "#fff",
    paddingHorizontal: 4,
    paddingVertical: 4,
    borderRadius: 20,
  },
  linearGradientBackdrop: {
    position: "absolute",
    height: height / 2.7,
    top: 0,
    left: 0,
    right: 0,
    zIndex: 0,
  },
});
