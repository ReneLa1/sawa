/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/self-closing-comp */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable quotes */
import React from "react";
import { Dimensions, StyleSheet, Text, View } from "react-native";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { ActionButton, Container, Typography } from "../../common";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
const { height: SCREEN_HEIGHT } = Dimensions.get("window");

const AnimatedTotal = ({ ticket }) => {
  return (
    <View style={[styles.containerStyles]}>
      <Container
        row
        center
        space="space-between"
        bgColor="#fff"
        customStyles={styles.wrapperStyles}>
        <Container column middle customStyles={{ height: "100%" }}>
          <Text style={styles.labelStyles}>Total</Text>
          <Container row center>
            <Typography variant="h1" customStyles={styles.priceStyles}>
              {ticket.price}
            </Typography>
            <Typography
              variant="title"
              color="##B3B6B7"
              customStyles={{ fontSize: 16 }}>
              rw
            </Typography>
          </Container>
        </Container>
        <ActionButton customStyles={styles.btnStyles}>
          <Typography variant="title" customStyles={[{ fontSize: 18 }]}>
            Continue
          </Typography>
        </ActionButton>
      </Container>
    </View>
  );
};
export default AnimatedTotal;

const styles = StyleSheet.create({
  containerStyles: {
    backgroundColor: "rgba(49, 172, 101,0.9)",
    // height: 60,
    position: "absolute",
    width: "100%",
    bottom: 0,
    ...ifIphoneX({ paddingBottom: 20 }, { paddingBottom: 10 }),
    paddingTop: 10,
    paddingHorizontal: 10,
  },
  wrapperStyles: {
    width: "100%",
    paddingVertical: 8,
    paddingHorizontal: 15,
    borderRadius: 5,
  },
  labelStyles: {
    fontWeight: "500",
    color: "##B3B6B7",
    // marginBottom: 5,
  },
  priceStyles: {
    fontSize: 25,
    marginRight: 3,
    color: "#000",
  },
  btnStyles: {
    backgroundColor: "rgba(241, 196, 15,1)",
    paddingRight: 10,
    paddingLeft: 10,
    paddingTop: 15,
    paddingBottom: 15,
    width: wp(50),
    borderRadius: 5,
  },
});
