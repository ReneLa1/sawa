/* eslint-disable prettier/prettier */
/* eslint-disable quotes */
import { lightTheme } from "../../../common/utils/theme";
import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  theme: { ...lightTheme },
};

export const utilSlice = createSlice({
  name: "utils",
  initialState,
  reducers: {
    switchTheme: (state, action) => {
      state.theme = action.payload;
    },
  },
});

export const { switchTheme } = utilSlice.actions;

export default utilSlice.reducer;
