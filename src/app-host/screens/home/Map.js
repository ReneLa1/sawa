/* eslint-disable no-shadow */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable quotes */
import React, { useEffect, useRef, useState } from "react";
import { StyleSheet, View, Alert, Platform } from "react-native";
import MapView, { PROVIDER_GOOGLE } from "react-native-maps";
import Geolocation from "@react-native-community/geolocation";

const lightMap = [
  {
    featureType: "administrative.land_parcel",
    elementType: "labels",
    stylers: [
      {
        visibility: "off",
      },
    ],
  },
  {
    featureType: "poi",
    elementType: "labels.text",
    stylers: [
      {
        visibility: "off",
      },
    ],
  },
  {
    featureType: "poi.business",
    stylers: [
      {
        visibility: "off",
      },
    ],
  },
  {
    featureType: "poi.park",
    elementType: "labels.text",
    stylers: [
      {
        visibility: "off",
      },
    ],
  },
  {
    featureType: "road.local",
    elementType: "labels",
    stylers: [
      {
        visibility: "off",
      },
    ],
  },
];

const Map = () => {
  const _map = useRef(null);
  const [location, setLocation] = useState({
    latitude: -1.9401634437981434,
    longitude: 30.08007229308605,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
    heading: 0,
  });

  useEffect(() => {
    getCurrentLocation();
  }, []);

  const getCurrentLocation = () => {
    Geolocation.getCurrentPosition(
      position => {
        const { coords } = position;

        setLocation({
          latitude: coords.latitude,
          longitude: coords.longitude,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
          heading: coords.heading,
        });
      },
      error => Alert.alert("Error", JSON.stringify(error)),
      {
        //  enableHighAccuracy: true, timeout: 20000, maximumAge: 1000
      },
    );
    // this.watchID = Geolocation.watchPosition(position => {
    //   const lastPosition = JSON.stringify(position);
    //   this.setState({ lastPosition });
    // });
  };

  useEffect(() => {
    if (location) {
      const { latitude, longitude } = location;

      if (_map.current) {
        _map.current?.animateCamera(
          {
            center: {
              latitude,
              longitude,
            },
            zoom: 13,
          },
          3000,
        );
      }
    }
  }, [location]);

  if (location) {
    return (
      <MapView
        style={{
          ...StyleSheet.absoluteFillObject,
        }}
        ref={_map}
        customMapStyle={lightMap}
        showsUserLocation={true}
        zoomEnabled={true}
        // provider={Platform.OS === "android" && PROVIDER_GOOGLE}
        initialRegion={location}
      />
    );
  }
  return <View style={{ flex: 1, backgroundColor: "#1D1D22" }} />;
};

export default Map;
