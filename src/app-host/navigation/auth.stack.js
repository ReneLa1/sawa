/* eslint-disable quotes */
import {
  CardStyleInterpolators,
  createStackNavigator,
} from "@react-navigation/stack";
import React from "react";
import SignIn from "../screens/auth/Signin";
import Signup from "../screens/auth/Signup";
import ForgetPassword from "../screens/auth/ForgetPassword";
import ResetPassword from "../screens/auth/ResetPassword";
import { AppRoutes } from "./app.routes";

export const Stack = createStackNavigator();

export const AuthStack = props => {
  return (
    <Stack.Navigator
      screenOptions={({ navigation }) => ({
        cardStyle: {
          // backgroundColor: "#2a3039",
        },
      })}>
      <Stack.Screen
        name={AppRoutes.SIGN_IN}
        component={SignIn}
        initialParams={props.route.params}
        options={({}) => ({
          headerShown: false,
        })}
      />
      <Stack.Screen
        name={AppRoutes.SIGN_UP}
        component={Signup}
        initialParams={props.route.params}
        options={({}) => ({
          headerShown: false,
        })}
      />
      <Stack.Screen
        name={AppRoutes.FORGOT_PASSWORD}
        component={ForgetPassword}
        initialParams={props.route.params}
        options={({}) => ({
          headerShown: false,
        })}
      />
      <Stack.Screen
        name={AppRoutes.RESET_PASSWORD}
        component={ResetPassword}
        initialParams={props.route.params}
        options={({}) => ({
          headerShown: false,
        })}
      />
      {/* <Stack.Screen
        name={AppRoutes.VERIFY}
        // component={VerifyScreen}
        options={({ route, navigation }) => ({ headerShown: false })}
      /> */}
    </Stack.Navigator>
  );
};
