/* eslint-disable quotes */
export const AppRoutes = {
  //App intro routes
  INTRO_STACK: "intro stack",
  INTRO_SLIDER: "intro slider",
  APP_SERVICES: "app services",

  //un authenticated routes
  AUTH_STACK: "auth_stack",
  SIGN_IN: "sign_in",
  SIGN_UP: "sign_up",
  VERIFY: "verification",
  FORGOT_PASSWORD: "forgot password",
  RESET_PASSWORD: "reset password",

  //authenticated routes

  HOME_NAV: "home_navigator",

  DYNAMIC_SCREEN: "dynamic screen",

  //Bus service stack
  SAWA_BUS_STACK: "sawa bus stack",

  PUBLIC_BUS_STACK: "public bus stack",
  PUBLIC_BUS: "public bus",

  CHOOSE_LOCATION: "choose location",
  MAIN_HOME_STACK: "main home stack",
  MAIN_HOME: "main home",

  MAIN_ORDERS_STACK: "main orders stack",
  MAIN_ORDERS: "main orders",

  MAIN_ACCOUNT_STACK: "main account stack",
  MAIN_ACCOUNT: "main account",
  MANAGE_SERVICES: "manage services",

  BUS_NAVIGATOR: "bus_tab_navigator", //navigations on the bus_tab
  BUS_HOME_TAB: "bus_home_tab",
  BUS_HOME: "bus home ",
  AVAILABLE_TICKETS: "available tickets",
  SELECTED_TICKET: "selected ticket",
  PAY_NUMBER: "pay number",
  TRAVELLER_LIST: "traveller list",
  ADD_TRAVELLER: "add traveller",
  EDIT_TRAVELLER: "edit traveller",

  TRIPS_HISTORY_TAB: "trips history tab",
  TRIPS_HISTORY: "trips history",
  TICKET: "ticket",

  USER_TAB: "user tab",

  CARD_NAVIGATOR: "card_navigator", //navigations on the card_tab
  CARD_HOME: "card_index_screen",
  CARD_HISTORY: "card history",
  MY_CARDS: "my cards",
  ADD_CARD: "add card",
  TOP_UP: "top up",

  MOTO_TAB: "moto_tab_navigator", //navigations on the moto_tab
  MOTO_HOME: "moto_index_screen",
  SEARCH_LOCATION: "search_location",
  TRIP_HISTORY: "trip_history",
  DRIVER_MAP: "driver_search_and_location",
  TRIP_SUMMARY: "trip_summary",
  TAP_PAY: "tap&go_pay_screen", // it is a modal
  CHOOSE_CARD: "choose_pay_card",
  RATE_ME: "rate_me", // its a modal

  CAR_RENTAL_STACK: "car rental stack",
  CAR_RENTAL: "car rental",
  //   SEARCH_LOCATION: "search location",

  //wallet stack
  WALLET_STACK: "wallet stack",
  WALLET: "wallet",

  //user profile
  ACCOUNT_TAB: "account tab",

  PROFILE: "user_profile",
  EDIT_PROFILE: "edit_profile",

  //car hail
  CAR_HAIL_STACK: "car hail stack",
  CAR_HAIL_HOME: "car hail home",
  CAR_JOURNEY_SCREEN: "car journey screen",

  //settings stack
  SETTING_STACK: "settings_stack",
  SETTINGS_HOME: "settings_home",
  PRIVACY: "privacy_policy",
  ABOUT_US: "about_us",
};
