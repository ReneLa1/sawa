/* eslint-disable no-trailing-spaces */
/* eslint-disable react/self-closing-comp */
/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable quotes */
import { useNavigation } from "@react-navigation/native";
import React from "react";
import { Image, StyleSheet, Text } from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { AppRoutes } from "../navigation/app.routes";
import { ActionButton, Container } from "../../common";
import { useSelector } from "react-redux";
// import { selectAllServices } from "../redux/services/services.slice";
// const busIcon = require("../../../assets/icons/bus-icon.png");
// const tickIcon = require("../../../assets/icons/tick-icon.png");
// const cardIcon = require("../../../assets/icons/card-icon.png");
// const moveIcon = require("../../../assets/icons/move-icon.png");
// const taxiIcon = require("../../../assets/icons/taxi-icon.png");
// const rentIcon = require("../../../assets/icons/car-rent.png");
// const rideIcon = require("../../../assets/icons/ride-icon.png");
// const shuriIcon = require("../../../assets/icons/shuri.png");

const Services = () => {
  const navigation = useNavigation();
  const { services } = useSelector(state => state.Services);
  const perChunk = 4;

  var newServices = services?.reduce((resArray, service, index) => {
    const chunkIndex = Math.floor(index / perChunk);
    if (!resArray[chunkIndex]) {
      resArray[chunkIndex] = [];
    }

    resArray[chunkIndex].push(service);

    return resArray;
  }, []);

  return (
    <Container row customStyles={styles.containerStyles}>
      {newServices?.map((chunk, idx) => {
        return (
          <Container
            key={idx}
            row
            center
            space="space-between"
            customStyles={{ marginBottom: 15 }}>
            {chunk.map((service, i) => {
              return Service(service.icon_link, service.title, () => {
                navigation.navigate(AppRoutes.DYNAMIC_SCREEN, { service });
              });
            })}
          </Container>
        );
      })}
    </Container>
  );
};

const Service = (url, title, onPress) => {
  return (
    <ActionButton
      onPress={onPress}
      customStyles={[styles.serviceWrapperStyles]}
      activeScale={0.95}
      tension={50}
      friction={7}
      useNativeDriver>
      <Image
        source={{ uri: url }}
        style={styles.iconStyles}
        resizeMode="contain"
      />
      <Text
        style={{
          fontSize: 11,
          color: "#fff",
          //   fontWeight: "600",
          fontFamily: "ArialRoundedMTBold",
        }}>
        {title}
      </Text>
    </ActionButton>
  );
};
export default Services;

const styles = StyleSheet.create({
  containerStyles: {
    // flexWrap: "wrap",
    paddingVertical: 15,
    paddingHorizontal: 15,
    marginTop: hp(12),
  },
  serviceWrapperStyles: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    width: 79,
    height: 70,
    borderRadius: 10,
    backgroundColor: "rgba(0, 0, 0, 0.2)",
    marginRight: 7,
  },
  iconStyles: {
    width: 25,
    height: 25,
    marginBottom: 10,
    aspectRatio: 1,
  },
});
