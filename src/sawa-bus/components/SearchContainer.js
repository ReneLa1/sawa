/* eslint-disable quotes */
/* eslint-disable react/self-closing-comp */

import { useNavigation } from "@react-navigation/native";
import { format } from "date-fns";
import React from "react";
import { StyleSheet, Text } from "react-native";
import Animated, { FadeInDown } from "react-native-reanimated";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { useSelector } from "react-redux";
import { ActionButton, Container } from "../../common";
import { BusRoutes } from "../navigation/bus.routes";

const SearchContainer = ({ loading }) => {
  const navigation = useNavigation();
  const { trip } = useSelector(state => state.Trip);

  return (
    <Animated.View
      entering={FadeInDown.springify().delay(5)}
      style={styles.containerStyles}>
      {FieldBtn(
        "from",
        trip.from ? trip.from : "Depart",
        () => {
          navigation.navigate(BusRoutes.SEARCH_STATIONS, {
            searchType: "from",
          });
        },
        loading,
      )}
      <Container customStyles={styles.dividerStyles} />
      {FieldBtn(
        "to",
        trip.to ? trip.to : "Arrival station",
        () => {
          navigation.navigate(BusRoutes.SEARCH_STATIONS, { searchType: "to" });
        },
        loading,
      )}
      <Container customStyles={styles.dividerStyles} />
      {FieldBtn(
        "Date",
        trip.date
          ? `${format(trip.date, "dd-MM-yyyy    HH:mm")}`
          : `${format(new Date(), "dd-MM-yyyy    HH:mm")}`,
        () => {
          navigation.navigate(BusRoutes.CHOOSE_DATE);
        },
        loading,
      )}
    </Animated.View>
  );
};

const FieldBtn = (label, title, onPress, loading) => {
  return (
    <ActionButton
      disabled={loading}
      onPress={onPress}
      customStyles={[styles.fieldBtnStyles]}
      activeScale={0.95}
      tension={50}
      friction={7}
      useNativeDriver>
      <Text style={styles.labelStyles}>{label}</Text>
      <Text style={styles.titleStyles}>{title}</Text>
    </ActionButton>
  );
};
export default SearchContainer;

const styles = StyleSheet.create({
  containerStyles: {
    height: hp(30),
    width: "100%",
    borderRadius: 10,
    flexDirection: "column",
    justifyContent: "space-evenly",
    backgroundColor: "#FDFEFE",
    padding: 20,
  },
  dividerStyles: {
    borderBottomWidth: 1,
    borderRadius: 1,
    borderBottomColor: "#e6e8ec",
    width: "100%",
  },
  fieldBtnStyles: {
    flexDirection: "column",
    width: "100%",
    paddingHorizontal: 10,
    paddingVertical: 10,
    alignItems: "flex-start",
  },
  labelStyles: {
    fontSize: 12,
    color: "#7B7D7D",
    marginTop: 10,
    marginBottom: 12,
    fontWeight: "600",
    // fontFamily: "ArialRoundedMTBold",
  },
  titleStyles: {
    fontSize: 18,
    color: "#313131",
    marginBottom: 7,
    //   fontWeight: "600",
    fontFamily: "ArialRoundedMTBold",
  },
});
