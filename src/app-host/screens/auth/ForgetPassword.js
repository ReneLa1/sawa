/* eslint-disable react/jsx-no-comment-textnodes */
/* eslint-disable no-trailing-spaces */
/* eslint-disable react/self-closing-comp */
/* eslint-disable prettier/prettier */
/* eslint-disable quotes */
/* eslint-disable react-native/no-inline-styles */

import { useNavigation } from "@react-navigation/native";
import React, { useEffect, useState } from "react";
import { Keyboard, Platform, StyleSheet, View } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import Entypo from "react-native-vector-icons/Entypo";
import Feather from "react-native-vector-icons/Feather";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Fontisto from "react-native-vector-icons/Fontisto";
import Ionicons from "react-native-vector-icons/Ionicons";
import {
  ActionButton,
  Container,
  Input,
  PrimaryButton,
  Typography,
} from "../../../common";
import { AppRoutes } from "../../navigation/app.routes";

const CardIcon = (
  <FontAwesome name="credit-card-alt" size={24} color="rgb(125, 206, 160)" />
);

const MotoIcon = <Fontisto name="motorcycle" size={24} color="#3498DB" />;

const CarIcon = (
  <Ionicons name="car-sport-sharp" size={28} color="rgba(239, 70, 111,0.9)" />
);
const BusIcon = <Fontisto name="bus" size={24} color="rgb(52, 152, 219 )" />;
const PhoneIcon = <Feather name="phone" size={24} color="#3498DB" />;
const LockIcon = <Feather name="lock" size={24} color="#3498DB" />;
const CloseIcon = <Entypo name="chevron-left" size={24} color="#fff" />;
const EmailIcon = <Feather name="mail" size={24} color="#3498DB" />;

const ForgetPassword = () => {
  const navigation = useNavigation();
  const insets = useSafeAreaInsets();
  const [keyShown, setKeyboardShow] = useState(false);

  useEffect(() => {
    return () => {
      Keyboard.remove("keyboardDidShow", _keyboardDidShow);
      Keyboard.remove("keyboardDidHide", _keyboardDidHide);
    };
  }, []);

  const _keyboardDidShow = () => {
    setKeyboardShow(true);
  };

  const _keyboardDidHide = () => {
    setKeyboardShow(false);
  };

  return (
    <KeyboardAwareScrollView
      showsVerticalScrollIndicator={false}
      style={{}}
      resetScrollToCoords={{ x: 0, y: 0 }}
      contentContainerStyle={{ flex: 1 }}
      scrollEnabled={keyShown}>
      <View style={styles.containerStyles}>
        <ActionButton
          onPress={() => navigation.pop()}
          customStyles={{
            ...styles.closeBtnStyles,
            top: Platform.OS === "ios" ? insets.top - 30 : insets.top + 10,
          }}>
          {CloseIcon}
        </ActionButton>
        <Typography variant="h2" customStyles={{ fontSize: 19, top: -20 }}>
          Reset Password
        </Typography>
        <Typography variant="h2" customStyles={{ fontSize: 16, top: -10 }}>
          Enter email and phone number associated with your account. We'll send
          a verification code to you reset your password
        </Typography>
      </View>

      {/* for container  */}
      <Container
        middle
        flex={0.3}
        customStyles={{ padding: 20 }}
        bgColor={"#F8F9F9"}>
        <Input
          placeholder="E-mail"
          placeholderTextColor={"#7B7D7D"}
          inputWrapperStyles={styles.inputWrapper}
          inputStyles={styles.inputStyles}
          maxLength={12}
          customStyles={{ marginBottom: 15 }}
          enablesReturnKeyAutomatically
          icon={EmailIcon}
        />
        <Input
          placeholder="phone number"
          placeholderTextColor={"#7B7D7D"}
          inputWrapperStyles={styles.inputWrapper}
          inputStyles={styles.inputStyles}
          maxLength={12}
          keyboardType="numeric"
          customStyles={{ marginBottom: 15 }}
          enablesReturnKeyAutomatically
          icon={PhoneIcon}
        />
      </Container>

      {/* footer */}
      <Container
        flex={0.3}
        space="space-between"
        customStyles={{ paddingHorizontal: 20, paddingBottom: 10 }}
        bgColor={"#F8F9F9"}>
        <PrimaryButton
          onPress={() => navigation.navigate(AppRoutes.RESET_PASSWORD)}
          customStyles={{
            borderRadius: 4,
            flexDirection: "row",
          }}>
          <Typography
            variant="title"
            color="#fff"
            customStyles={{ fontSize: 17 }}>
            Request Code
          </Typography>
        </PrimaryButton>
      </Container>
    </KeyboardAwareScrollView>
  );
};

export default ForgetPassword;

const styles = StyleSheet.create({
  containerStyles: {
    width: "100%",
    flex: 0.4,
    flexDirection: "column",
    justifyContent: "flex-end",
    paddingHorizontal: 30,
    paddingBottom: 20,
    backgroundColor: "#E5E7E9",
  },
  closeBtnStyles: {
    position: "absolute",
    left: 10,
    height: 40,
    width: 40,
    borderRadius: 40 / 2,
    backgroundColor: "rgba(33, 47, 61,0.5)",
  },
  inputWrapper: {
    backgroundColor: "transparent",
    borderBottomColor: "#313131",
    borderBottomWidth: 0.4,
  },
  inputStyles: {
    fontFamily: "ArialRoundedMTBold",
    color: "#313131",
  },
});
