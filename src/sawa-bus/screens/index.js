/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable quotes */
/* eslint-disable react-native/no-inline-styles */
import LottieView from "lottie-react-native";
import React, { useEffect, useRef } from "react";
import { ActivityIndicator, StyleSheet } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { Container, PrimaryButton, Typography } from "../../common";
import CloseService from "../../common/CloseService";
import GradientBackground from "../components/GradientBackground";
import SearchContainer from "../components/SearchContainer";
import {
  useGetStartStationsQuery,
  useGetStopStationsQuery,
} from "../redux/stations.slice";
import { useSelector } from "react-redux";
import { useGetAvailableBusesMutation } from "../redux/trip.slice";
import { useNavigation } from "@react-navigation/native";
import { BusRoutes } from "../navigation/bus.routes";
import finalPropsSelectorFactory from "react-redux/es/connect/selectorFactory";

const BusLogo = () => {
  const load_logo = useRef();
  useEffect(() => {
    if (load_logo.current) {
      load_logo.current.play();
    }
  }, []);
  return (
    <LottieView
      ref={load_logo}
      style={{
        width: 70,
        height: 70,
        backgroundColor: "transparent",
      }}
      source={require("../../../assets/animations/ticket_anim1.json")}
      autoPlay
      loop={false}
      speed={1}
      autoSize
      resizeMode="cover"
    />
  );
};

const SawaBus = ({}) => {
  const insets = useSafeAreaInsets();
  const navigation = useNavigation();
  const { trip } = useSelector(state => state.Trip);

  useGetStartStationsQuery();
  useGetStopStationsQuery();

  return (
    <Container
      flex={1}
      bgColor={"#F2F4F4"}
      customStyles={[{ paddingTop: insets.top }]}>
      <GradientBackground />
      <Container
        row
        space="space-between"
        customStyles={{
          width: "100%",
          paddingHorizontal: 15,
          zIndex: 2,
        }}>
        <Container row center>
          <BusLogo />
        </Container>
        <CloseService />
      </Container>
      <Container column customStyles={styles.wrapperStyles}>
        <SearchContainer />
        <Container customStyles={{ paddingHorizontal: 15, paddingTop: 30 }}>
          <PrimaryButton
            disabled={!trip.from || !trip.to}
            onPress={() => navigation.navigate(BusRoutes.AVAILABLE_BUSES)}
            //#31AC65
            bgColor={
              trip.from && trip.to
                ? "rgba(49, 172, 101,0.9)"
                : "rgba(49, 172, 101,0.5)"
            }
            activeScale={0.95}
            tension={50}
            friction={7}
            useNativeDriver
            customStyles={{
              borderRadius: 25,
              paddingTop: 15,
              paddingBottom: 15,
            }}>
            <Typography
              variant="caption"
              customStyles={{
                color: "#fff",
                fontSize: 17,
              }}>
              Find buses and prices
            </Typography>
          </PrimaryButton>
        </Container>
      </Container>
    </Container>
  );
};

export default SawaBus;

const styles = StyleSheet.create({
  wrapperStyles: {
    flex: 1,
    zIndex: 2,
    paddingHorizontal: 20,
    paddingTop: 100,
  },
});
