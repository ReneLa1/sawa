/* eslint-disable react-native/no-inline-styles */
/* eslint-disable quotes */
// import { ChunkManager } from "@callstack/repack/client";
import { useNavigation, useRoute } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { StyleSheet } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import AntDesign from "react-native-vector-icons/AntDesign";
import Feather from "react-native-vector-icons/Feather";
import { ActionButton, Container } from "../../common";
import PageLoader from "../../common/PageLoader";
import BusStack from "../../sawa-bus/navigation/BusStack";
import { AppRoutes } from "./app.routes";

// ChunkManager.configure({
//   resolveRemoteChunk: async chunkId => {
//     return {
//       url: `https://res.cloudinary.com/dsubmyluz/image/upload/v1654175206/Sawa/${chunkId}`,
//     };
//   },
// });

// const SawaBusModule = React.lazy(() => {
//   return Promise.all([
//     import(
//       /* webpackChunkName: "SawaBus" */ "../../sawa-bus/navigation/BusStack"
//     ),
//     new Promise(resolve =>
//       setTimeout(resolve, Math.floor(Math.random() * (1000 - 600 + 1)) + 600),
//     ),
//   ]).then(([moduleExports]) => moduleExports);
// });
// const CloseBtn = ({}) => {
//   const navigation = useNavigation();
//   return (
//     <Container
//       row
//       center
//       space="space-around"
//       customStyles={styles.closeBtnStyles}>
//       <ActionButton>
//         <Feather name="more-horizontal" size={20} color="black" />
//       </ActionButton>
//       <ActionButton onPress={() => navigation.goBack()}>
//         <AntDesign name="closecircleo" size={20} color="black" />
//       </ActionButton>
//     </Container>
//   );
// };

// const SawaBusStack = () => {
//   const insets = useSafeAreaInsets();
//   const {
//     params: { service },
//   } = useRoute();
//   return (
//     <React.Suspense
//       fallback={
//         <Container flex={1} center middle customStyles={styles.loaderContainer}>
//           <Container
//             row
//             customStyles={{
//               position: "absolute",
//               top: insets.top,
//               right: 15,
//             }}>
//             <CloseBtn />
//           </Container>
//           <PageLoader />
//         </Container>
//       }>
//       <SawaBusModule service={service} />
//     </React.Suspense>
//   );
// };

const Stack = createStackNavigator();

export const ModuleNavigator = () => {
  const {
    params: { service },
  } = useRoute();
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen
        name={AppRoutes.SAWA_BUS_STACK}
        component={BusStack}
        initialParams={{ service }}
      />
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({
  loaderContainer: {
    backgroundColor: "#fff",
    height: "100%",
    width: "100%",
  },
  closeBtnStyles: {
    height: 30,
    backgroundColor: "#fff",
    paddingHorizontal: 4,
    paddingVertical: 4,
    borderRadius: 20,
  },
});
