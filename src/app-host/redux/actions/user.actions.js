/* eslint-disable quotes */
import axios from "axios";
import { on_login } from "../../../common/utils/auth";
import actionTypes from "./types";
import { loginComplete, loginInProgress, userInValid } from "./ui.actions";
import { rootUrl } from "../../../common/utils/api";

export const saveCurrentUser = user => {
  return dispatch => {
    dispatch({ type: actionTypes.SAVE_CURRENT_USER, payload: user });
  };
};

export const requestUserLocation = location => ({
  type: actionTypes.USER_LOCATION_REQUEST_SUCCESS,
  payload: location,
});

//auth actions
export const loginUser = userCredentials => {
  return dispatch => {
    console.log(rootUrl);
    dispatch(loginInProgress());

    var data = new FormData();
    data.append("phone", userCredentials.phone);
    data.append("password", userCredentials.password);

    axios
      .post(
        `${rootUrl}/mobile/authenticate`,
        { phone: userCredentials.phone, password: userCredentials.password },
        {
          headers: {
            platform: "android",
            app_version: "v2",
          },
        },
      )
      .then(res => {
        if (res.status === 200) {
          const { data } = res.data;
          dispatch(loginSuccess(data));
          dispatch(loginComplete());
          on_login(data);
        }
      })
      .catch(err => {
        dispatch(loginComplete());
        dispatch({ type: "error", payload: err });
        const { data } = err.response.data;
        const newError = data ? data : "Credentials not correct. Try again";
        dispatch({ type: "error", payload: err });
        // console.log(err);
        dispatch(userInValid());
        dispatch(loginFail(newError));
      });
  };
};

export const loginSuccess = data => ({
  type: actionTypes.LOGIN_SUCCESS,
  payload: {
    user: data,
    token: data.token,
  },
});

export const loginFail = err => ({
  type: actionTypes.LOGIN_FAIL,
  payload: err,
});
