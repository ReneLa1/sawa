/* eslint-disable quotes */
export const BusRoutes = {
  //Bus service stack
  SAWA_BUS: "sawa bus home",
  SEARCH_STATIONS: "search stations",
  CHOOSE_DATE: "choose date",
  AVAILABLE_BUSES: "available buses schedule",
  CHECKOUT_TICKET: "check out ticket",
};
